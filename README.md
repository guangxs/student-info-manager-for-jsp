# Student Info Manager For JSP


#### 介绍
JSP大作业，学生信息管理系统。


## 系统为完全源码，如有疑问可联系：

![](doc/img/微信.jpg)

#### 软件架构
软件架构说明，由于本人作业要求是JSP，所以这里使用JSP结合其他开发技术
可概括技术点
1. SpringBoot
2. MySQL
3. JSP
4. Vue
5. Element UI
6. Axios

##### 技术要点

**类**前后端分离开发模式，MVC架构。系统虽然使用JSP进行页面开发，但在架构模式上，让jsp职能放在“页面”的定位上，并没有在jsp中进行过多的业务逻辑处理，只进行数据展示效果，相应的，具体的业务写在java代码中。

同时系统在Jsp中使用了Vue，能够很好地避免我们重复造轮子，提高了开发效率。

- MySQL 57
- Spring Data JPA
- Hutool
- lombok
- Jsp
- Vue 2x
- Axios
- Element UI
- Vant UI

#### 安装教程

##### 导入项目

###### 手动导入法
1.  下载本仓库或者 git clone https://gitee.com/zhimao/student-info-manager-for-jsp.git 到本地
2.  IDEA或Eclipse导入项目

###### Git导入法

> 本教程省略，建议大神使用

##### 创建数据库
1. 创建数据库xi_sims，并在src/main/resources/application.properties中配置，当然你也可以自定义数据库，大神请自行在下图配置
   ![](doc/img/配置数据库.png)

2. 启动类src/main/java/org/xi/AppRun.java，按照图示指示启动项目
   ![](doc/img/运行.png)

3. 如果报以下错误，说明项目没有对上数据库，请自行检查您的数据库配置，包括但不限于：数据库用户名密码/数据库类型/库名/..等
   ![](doc/img/未知数据库.png)

4. 启动成功，如果出现以下界面证明项目启动成功
   ![](doc/img/运行成功.png)

#### 使用说明

##### 环境要求

1.  JDK 8 or JDK 11 （其他版本未测试）。
2.  Eclipse 或 IntelliJ IDEA （都需要安装lombok插件，请根据自己使用的工具百度lombok安装，例如你使用的是Eclipse，就在百度搜索：Eclipse安装lombok插件）。
3.  建议使用MySQL 57，非必须，该系统支持多种数据库环境。
4.  maven项目，一般情况下需要配置。
5. 大神或根据自定义修改配置/环境


#### 项目预览

##### 登录页
![](doc/img/登录页.png)

##### 学生端个人页
![](doc/img/学生端个人页.png)

##### 学生端班级页
![](doc/img/学生端班级页.png)

##### 教师端个人信息
![](doc/img/教师端个人信息.png)

##### 教师端课程信息管理
![](doc/img/教师端教学课程成绩管理.png)

##### 教师端班级管理
![](doc/img/教师端班级管理.png)

##### 教师端编辑学生信息
![](doc/img/教师端编辑学生信息.png)

##### 管理员端排课管理编辑排课
![](doc/img/管理员端排课管理编辑排课.png)

##### 管理员端教室管理
![](doc/img/管理员端教室管理页.png)

##### 管理员端教师管理
![](doc/img/管理员端教师管理页.png)

##### 管理员端课程管理页
![](doc/img/管理员端课程管理页.png)

