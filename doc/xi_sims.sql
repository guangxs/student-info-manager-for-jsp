/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : localhost:3306
 Source Schema         : xi_sims

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : 65001

 Date: 11/07/2023 13:45:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sims_achievement
-- ----------------------------
DROP TABLE IF EXISTS `sims_achievement`;
CREATE TABLE `sims_achievement`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) DEFAULT NULL,
  `update_time` datetime(0) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `score` double DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sims_achievement
-- ----------------------------
INSERT INTO `sims_achievement` VALUES (1, '2021-12-09 13:16:34', '2021-12-09 13:16:34', 4, 100, 1);
INSERT INTO `sims_achievement` VALUES (2, '2021-12-09 13:16:35', '2021-12-09 13:16:35', 4, 97, 2);
INSERT INTO `sims_achievement` VALUES (3, '2021-12-09 13:16:35', '2021-12-09 13:16:35', 4, 50, 3);
INSERT INTO `sims_achievement` VALUES (4, '2021-12-09 13:16:36', '2021-12-09 13:16:36', 4, 70, 4);

-- ----------------------------
-- Table structure for sims_admin
-- ----------------------------
DROP TABLE IF EXISTS `sims_admin`;
CREATE TABLE `sims_admin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) DEFAULT NULL,
  `update_time` datetime(0) DEFAULT NULL,
  `number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `phone` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sims_admin
-- ----------------------------
INSERT INTO `sims_admin` VALUES (1, '2021-12-09 12:52:39', '2021-12-09 12:52:39', 'admin', '6feb54b3baa72ea450cf91456b9ca588', '13429944395');

-- ----------------------------
-- Table structure for sims_course
-- ----------------------------
DROP TABLE IF EXISTS `sims_course`;
CREATE TABLE `sims_course`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) DEFAULT NULL,
  `update_time` datetime(0) DEFAULT NULL,
  `credit` int(11) DEFAULT NULL,
  `learn_time` int(11) DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `number` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sims_course
-- ----------------------------
INSERT INTO `sims_course` VALUES (1, '2021-12-09 12:54:15', '2021-12-09 12:54:15', 4, 12, '计算机组成原理', 'KC1001');
INSERT INTO `sims_course` VALUES (2, '2021-12-09 12:54:51', '2021-12-09 12:54:51', 4, 12, 'C#教学实践', 'KC1002');
INSERT INTO `sims_course` VALUES (3, '2021-12-09 12:55:17', '2021-12-09 13:05:07', 4, 12, '安卓开发基础', 'KC1003');
INSERT INTO `sims_course` VALUES (4, '2021-12-09 12:56:08', '2021-12-09 12:56:08', 4, 12, 'SQL Server 数据库教程', 'KC1004');

-- ----------------------------
-- Table structure for sims_grade
-- ----------------------------
DROP TABLE IF EXISTS `sims_grade`;
CREATE TABLE `sims_grade`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) DEFAULT NULL,
  `update_time` datetime(0) DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `notice` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `number` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sims_grade
-- ----------------------------
INSERT INTO `sims_grade` VALUES (4, '2021-12-09 13:00:37', '2021-12-09 13:07:12', '计科一班', '同学们钉钉打开开始啦！', '5193001', 1);

-- ----------------------------
-- Table structure for sims_leader
-- ----------------------------
DROP TABLE IF EXISTS `sims_leader`;
CREATE TABLE `sims_leader`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `birth` datetime(0) DEFAULT NULL,
  `create_time` datetime(0) DEFAULT NULL,
  `email` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `leader_type` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `nickname` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `phone` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sex` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `update_time` datetime(0) DEFAULT NULL,
  `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sims_leader
-- ----------------------------
INSERT INTO `sims_leader` VALUES (1, '遥遥领先大学', 'https://avatar.csdnimg.cn/E/F/1/3_sr02020_1583460004.jpg', '2021-11-30 13:31:22', '2021-11-30 13:31:22', '1475197292@qq.com', '教师', '彭寒冬恨旋', '1001', '6feb54b3baa72ea450cf91456b9ca588', '13429944395', '男', '2021-11-30 13:31:22', '华霜雪');
INSERT INTO `sims_leader` VALUES (2, '遥遥领先大学', 'https://avatar.csdnimg.cn/E/F/1/3_sr02020_1583460004.jpg', '2021-11-30 13:31:22', '2021-11-30 13:31:22', '1475197292@qq.com', '教师', '葛雪觅雁玉', '1002', '6feb54b3baa72ea450cf91456b9ca588', '13429944395', '女', '2021-11-30 13:31:22', '东方');
INSERT INTO `sims_leader` VALUES (3, '遥遥领先大学', 'https://avatar.csdnimg.cn/E/F/1/3_sr02020_1583460004.jpg', '2021-11-30 13:31:22', '2021-11-30 13:31:22', '1475197292@qq.com', '教师', '戚书代翠亦', '1003', '6feb54b3baa72ea450cf91456b9ca588', '13429944395', '男', '2021-11-30 13:31:22', '杨海夏');
INSERT INTO `sims_leader` VALUES (4, '遥遥领先大学', 'https://avatar.csdnimg.cn/E/F/1/3_sr02020_1583460004.jpg', '2021-11-30 13:31:22', '2021-11-30 13:31:22', '1475197292@qq.com', '教师', '谢云凡夏山', '1004', '6feb54b3baa72ea450cf91456b9ca588', '13429944395', '女', '2021-11-30 13:31:22', '钱青玉');
INSERT INTO `sims_leader` VALUES (5, '遥遥领先大学', 'https://avatar.csdnimg.cn/E/F/1/3_sr02020_1583460004.jpg', '2021-11-30 13:31:22', '2021-11-30 13:31:22', '1475197292@qq.com', '教师', '苏乐念代雅', '1005', '6feb54b3baa72ea450cf91456b9ca588', '13429944395', '男', '2021-11-30 13:31:22', '孙雪');
INSERT INTO `sims_leader` VALUES (6, '遥遥领先大学', 'https://avatar.csdnimg.cn/E/F/1/3_sr02020_1583460004.jpg', '2021-11-30 13:31:22', '2021-11-30 13:31:22', '1475197292@qq.com', '教师', '苏山靖波雪', '1006', '6feb54b3baa72ea450cf91456b9ca588', '13429944395', '女', '2021-11-30 13:31:22', '彭雅');
INSERT INTO `sims_leader` VALUES (7, '遥遥领先大学', 'https://avatar.csdnimg.cn/E/F/1/3_sr02020_1583460004.jpg', '2021-11-30 13:31:22', '2021-11-30 13:31:22', '1475197292@qq.com', '教师', '周枫旋雪安', '1007', '6feb54b3baa72ea450cf91456b9ca588', '13429944395', '男', '2021-11-30 13:31:22', '戚从');
INSERT INTO `sims_leader` VALUES (8, '遥遥领先大学', 'https://avatar.csdnimg.cn/E/F/1/3_sr02020_1583460004.jpg', '2021-11-30 13:31:22', '2021-11-30 13:31:22', '1475197292@qq.com', '教师', '魏靖海萱寒', '1008', '6feb54b3baa72ea450cf91456b9ca588', '13429944395', '女', '2021-11-30 13:31:22', '奚冬代');
INSERT INTO `sims_leader` VALUES (9, '遥遥领先大学', 'https://avatar.csdnimg.cn/E/F/1/3_sr02020_1583460004.jpg', '2021-11-30 13:31:23', '2021-11-30 13:31:23', '1475197292@qq.com', '教师', '金念香恨凌', '1009', '6feb54b3baa72ea450cf91456b9ca588', '13429944395', '男', '2021-11-30 13:31:23', '秦傲');
INSERT INTO `sims_leader` VALUES (10, '遥遥领先大学', 'https://avatar.csdnimg.cn/E/F/1/3_sr02020_1583460004.jpg', '2021-11-30 13:31:23', '2021-11-30 13:31:23', '1475197292@qq.com', '教师', '谢凡雁千宛', '1010', '6feb54b3baa72ea450cf91456b9ca588', '13429944395', '女', '2021-11-30 13:31:23', '孙旋蓝');

-- ----------------------------
-- Table structure for sims_major
-- ----------------------------
DROP TABLE IF EXISTS `sims_major`;
CREATE TABLE `sims_major`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime(0) DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `update_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sims_major
-- ----------------------------
INSERT INTO `sims_major` VALUES (1, 'xxx001', '2021-12-01 17:28:50', '沈雁南觅系', '2021-12-01 17:28:50');
INSERT INTO `sims_major` VALUES (2, 'xxx011', '2021-12-01 17:28:50', '水从雁从系', '2021-12-01 17:28:50');
INSERT INTO `sims_major` VALUES (3, 'xxx021', '2021-12-01 17:28:50', '喻笑柔半系', '2021-12-01 17:28:50');
INSERT INTO `sims_major` VALUES (4, 'xxx031', '2021-12-01 17:28:50', '韩绿紫怀系', '2021-12-01 17:28:50');
INSERT INTO `sims_major` VALUES (5, 'xxx041', '2021-12-01 17:28:50', '司马波冬系', '2021-12-01 17:28:50');
INSERT INTO `sims_major` VALUES (6, 'xxx051', '2021-12-01 17:28:50', '吴半傲梦系', '2021-12-01 17:28:50');
INSERT INTO `sims_major` VALUES (7, 'xxx061', '2021-12-01 17:28:50', '韩易雪傲系', '2021-12-01 17:28:50');
INSERT INTO `sims_major` VALUES (8, 'xxx071', '2021-12-01 17:28:50', '潘从从山系', '2021-12-01 17:28:50');
INSERT INTO `sims_major` VALUES (9, 'xxx081', '2021-12-01 17:28:50', '孙半卉蕾系', '2021-12-01 17:28:50');
INSERT INTO `sims_major` VALUES (10, 'xxx091', '2021-12-01 17:28:50', '施波翠傲系', '2021-12-01 17:28:50');

-- ----------------------------
-- Table structure for sims_room
-- ----------------------------
DROP TABLE IF EXISTS `sims_room`;
CREATE TABLE `sims_room`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) DEFAULT NULL,
  `update_time` datetime(0) DEFAULT NULL,
  `level` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `room` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sims_room
-- ----------------------------
INSERT INTO `sims_room` VALUES (1, '2021-12-09 12:56:30', '2021-12-09 12:56:30', '明德楼', 'MD101', 101);
INSERT INTO `sims_room` VALUES (2, '2021-12-09 12:56:39', '2021-12-09 12:56:39', '计科楼', 'JK102', 102);
INSERT INTO `sims_room` VALUES (3, '2021-12-09 12:56:47', '2021-12-09 12:56:47', '生科楼', 'SJ303', 303);
INSERT INTO `sims_room` VALUES (4, '2021-12-09 12:56:51', '2021-12-09 12:56:51', '计科楼', 'JK202', 202);

-- ----------------------------
-- Table structure for sims_student
-- ----------------------------
DROP TABLE IF EXISTS `sims_student`;
CREATE TABLE `sims_student`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) DEFAULT NULL,
  `update_time` datetime(0) DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `birth` datetime(0) DEFAULT NULL,
  `email` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `grade_id` int(11) DEFAULT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `nickname` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `phone` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `position` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sex` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `major` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sims_student
-- ----------------------------
INSERT INTO `sims_student` VALUES (1, '2021-12-09 13:07:49', '2021-12-09 13:14:58', '湖北省/孝感市/孝南区', NULL, '1999-12-26 08:00:00', '1475197292@qq.com', 4, '谭雨茜', '吃肉肉长肉肉', '518300214421', '6feb54b3baa72ea450cf91456b9ca588', '13429944395', '学生', '女', NULL);
INSERT INTO `sims_student` VALUES (2, '2021-12-09 13:08:42', '2021-12-09 13:08:42', NULL, NULL, NULL, NULL, 4, '张三', '醉酒', '519300214101', '6feb54b3baa72ea450cf91456b9ca588', NULL, '班长', '男', NULL);
INSERT INTO `sims_student` VALUES (3, '2021-12-09 13:09:02', '2021-12-09 13:09:02', NULL, NULL, NULL, NULL, 4, '李四', '长歌', '519300214102', '6feb54b3baa72ea450cf91456b9ca588', NULL, '团委', '男', NULL);
INSERT INTO `sims_student` VALUES (4, '2021-12-09 13:09:40', '2021-12-09 13:09:40', NULL, NULL, NULL, NULL, 4, '王五', '是风动', '519300214103', '6feb54b3baa72ea450cf91456b9ca588', NULL, '学生', '女', NULL);

-- ----------------------------
-- Table structure for sims_teacher
-- ----------------------------
DROP TABLE IF EXISTS `sims_teacher`;
CREATE TABLE `sims_teacher`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) DEFAULT NULL,
  `update_time` datetime(0) DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `birth` datetime(0) DEFAULT NULL,
  `leader` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `phone` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sex` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sims_teacher
-- ----------------------------
INSERT INTO `sims_teacher` VALUES (1, '2021-12-09 12:57:52', '2021-12-09 12:57:52', NULL, NULL, NULL, '教授', '丁洁', 'J1001', '6feb54b3baa72ea450cf91456b9ca588', NULL, '女');
INSERT INTO `sims_teacher` VALUES (2, '2021-12-09 12:58:14', '2021-12-09 12:58:14', NULL, NULL, NULL, '讲师', '黄兰英', 'J1002', '6feb54b3baa72ea450cf91456b9ca588', NULL, '女');
INSERT INTO `sims_teacher` VALUES (3, '2021-12-09 13:04:12', '2021-12-09 13:04:12', NULL, NULL, NULL, '讲师', '李骥', 'J1003', '6feb54b3baa72ea450cf91456b9ca588', NULL, '男');
INSERT INTO `sims_teacher` VALUES (4, '2021-12-09 13:04:38', '2021-12-09 13:04:38', NULL, NULL, NULL, '讲师', '祝攀', 'J1004', '6feb54b3baa72ea450cf91456b9ca588', NULL, '男');

-- ----------------------------
-- Table structure for sims_timetable
-- ----------------------------
DROP TABLE IF EXISTS `sims_timetable`;
CREATE TABLE `sims_timetable`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) DEFAULT NULL,
  `update_time` datetime(0) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `end` int(11) DEFAULT NULL,
  `grade_id` int(11) DEFAULT NULL,
  `knob` int(11) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `start` int(11) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `week` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sims_timetable
-- ----------------------------
INSERT INTO `sims_timetable` VALUES (1, '2021-12-09 13:05:33', '2021-12-09 13:05:33', 1, 6, 4, 2, 1, 1, 3, 1);
INSERT INTO `sims_timetable` VALUES (2, '2021-12-09 13:06:01', '2021-12-09 13:06:01', 3, 12, 4, 3, 2, 2, 4, 1);
INSERT INTO `sims_timetable` VALUES (3, '2021-12-09 13:06:24', '2021-12-09 13:06:24', 4, 12, 4, 2, 2, 2, 1, 6);

SET FOREIGN_KEY_CHECKS = 1;
