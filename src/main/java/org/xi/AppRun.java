package org.xi;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * @author ：xi
 * @time ：Created in 17:13
 * @description：程序入口
 */
@EnableJpaAuditing
@SpringBootApplication
public class AppRun extends Run {
        public static void main(String[] args) {
            run(AppRun.class);
        }
}
