package org.xi;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.core.env.Environment;
import org.xi.sims.Init;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Slf4j
public class Run implements ApplicationRunner, CommandLineRunner {

    @Autowired
    private Init init;

    @Autowired
    private Environment env;
    public static void run(Class<?> cla)  {
        SpringApplication.run(cla);
    }



    /**
     * @desc <p> 启动成功时执行，先执行 </p>
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {
    	
    }


    /**
     * @desc <p> 启动成功时执行，后执行 </p>
     */
    @Override
    public void run(String... args) throws Exception {
        init.init();
    	 //获取配置的端口s
        String contextPath = env.getProperty("server.servlet.context-path");
        //IPUtil.getServerIp()获取本机IP，本机使用Hutool工具
        String port = env.getProperty("server.port");
        logInfo(port);
        try {
            Runtime.getRuntime().exec("cmd /c start " + "http://localhost:" + port + "/login");//在cmd里面执行命令唤起浏览器
        } catch (IOException e) {
            e.printStackTrace();//当try中语句有错误时执行这个
        }
    }

    private void logInfo(String port) throws UnknownHostException {
        log.info("\n" +
                "//                          _ooOoo_                               //\n" +
                "//                         o8888888o                              //\n" +
                "//                         88\" . \"88                              //\n" +
                "//                         (| ^_^ |)                              //\n" +
                "//                         O\\  =  /O                              //\n" +
                "//                      ____/`---'\\____                           //\n" +
                "//                    .'  \\\\|     |//  `.                         //\n" +
                "//                   /  \\\\|||  :  |||//  \\                        //\n" +
                "//                  /  _||||| -:- |||||-  \\                       //\n" +
                "//                  |   | \\\\\\  -  /// |   |                       //\n" +
                "//                  | \\_|  ''\\---/''  |   |                       //\n" +
                "//                  \\  .-\\__  `-`  ___/-. /                       //\n" +
                "//                ___`. .'  /--.--\\  `. . ___                     //\n" +
                "//              .\"\" '<  `.___\\_<|>_/___.'  >'\"\".                  //\n" +
                "//            | | :  `- \\`.;`\\ _ /`;.`/ - ` : | |                 //\n" +
                "//            \\  \\ `-.   \\_ __\\ /__ _/   .-` /  /                 //\n" +
                "//      ========`-.____`-.___\\_____/___.-`____.-'========         //\n" +
                "//                           `=---='                              //\n" +
                "//      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^        //\n" +
                "//             佛祖保佑          永无BUG         永不修改         //\n" +
                "\n");
   
        log.info("\n\t----------------------------------------------------------\n\t" +
                "Application Boot is running! Access URLs:\n\t" +
                "\thttp://"+ "127.0.0.1" +':' + port + "/\n\t" +
                "\thttp://"+ InetAddress.getLocalHost().getHostAddress() +':' + port  + "/\n\t" +
                "----------------------------------------------------------"
        );
    }
}
