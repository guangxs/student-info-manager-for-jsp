package org.xi.sims;

import cn.hutool.core.util.ObjectUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.xi.sims.common.Constants;
import org.xi.sims.pojo.entity.Admin;
import org.xi.sims.service.AdminService;

@Component
@RequiredArgsConstructor
public class Init {
    private final AdminService adminService;

    public void init() {
        Admin admin = adminService.findByNumber(Constants.ADMIN);
        if (ObjectUtil.isEmpty(admin)) {
            adminService.save(
                    Admin.builder()
                            .number(Constants.ADMIN)
                            .password("6feb54b3baa72ea450cf91456b9ca588")
                            .phone("13429944395")
                            .build()
            );
        }
    }
}
