package org.xi.sims.base.controller;

import cn.dev33.satoken.stp.StpUtil;

/**
 * @author ：xi
 * @time ：Created in 13:31
 * @description：
 */
public abstract class IAbstractController {
    public Integer loginId(){
//        IAssert.isFalse(StpUtil.isLogin(), ResultCode.UN_LOGIN);
//        Object oId = StpUtil.getLoginId();
//        return Integer.valueOf(String.valueOf(oId));
        return StpUtil.getLoginIdAsInt();
    }
}
