package org.xi.sims.base.controller;

import cn.dev33.satoken.annotation.SaCheckRole;
import cn.dev33.satoken.annotation.SaMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.xi.sims.base.service.IBaseService;
import org.xi.sims.common.Constants;
import org.xi.sims.common.result.Result;

/**
 * @author ：xi
 * @date ：Created in 15:35
 * @description：
 * @modified By：$
 * @version: $
 */
public class IBaseController<E, S extends IBaseService<E>> extends IAbstractController {
    /**
     * 动态 IOC代理
     *
     * 多个需要使用Autowired
     */
    @Autowired
    protected S service;

    @SaCheckRole(value = {Constants.ADMIN, Constants.TEACHER}, mode = SaMode.OR)
    @DeleteMapping("delete/{id}")
    public Result delete(@PathVariable(value = "id") Integer id) {
        service.deleteById(id);
        return Result.ok();
    }


}
