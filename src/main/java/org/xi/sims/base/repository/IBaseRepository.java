package org.xi.sims.base.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * @author ：xi
 * @time ：Created in 18:00
 * @description：
 */
@NoRepositoryBean
public interface IBaseRepository<E> extends JpaRepository<E, Integer>, JpaSpecificationExecutor<E> {
}
