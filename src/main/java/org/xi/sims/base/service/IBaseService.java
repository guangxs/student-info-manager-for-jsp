package org.xi.sims.base.service;


/**
 * @author ：xi
 * @time ：Created in 19:12
 * @description：
 */
public interface IBaseService<E>  {

    E findById(int id);

    void deleteById(int id);

    void deleteAll();

    E save(E e);
}
