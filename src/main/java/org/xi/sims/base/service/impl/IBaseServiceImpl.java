package org.xi.sims.base.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.xi.sims.base.repository.IBaseRepository;
import org.xi.sims.base.service.IBaseService;

import java.lang.reflect.ParameterizedType;
import java.util.Optional;

/**
 * @author ：xi
 * @time ：Created in 19:13
 * @description：
 */
public abstract class IBaseServiceImpl<E, R extends IBaseRepository<E>> implements IBaseService<E> {
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    protected R repository;

    /**
     * 当前泛型真实类型的Class
     */

    protected Class<E> entityClass;

    /**
     * 实体类的父类Class
     */
//    protected Class<?> baseEntityClass;


    public IBaseServiceImpl() {
        ParameterizedType pt = (ParameterizedType) this.getClass().getGenericSuperclass();
        entityClass = (Class<E>) pt.getActualTypeArguments()[0];
//        baseEntityClass = entityClass.getSuperclass();
    }

    @Override
    public E findById(int id) {
//        return repository.getById(id);
        Optional<E> e = repository.findById(id);
        return e.orElse(null);
    }

    @Override
    public void deleteById(int id) {
        repository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public E save(E e) {
        return repository.save(e);
    }
}
