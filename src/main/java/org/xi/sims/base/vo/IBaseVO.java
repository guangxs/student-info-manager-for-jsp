package org.xi.sims.base.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * @author ：xi
 * @time ：Created in 18:47
 * @description：
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public abstract class IBaseVO<V> implements Serializable {
    private static final long serialVersionUID = 1L;

    protected Integer id;
    @JsonFormat(pattern="yyyy-MM-dd hh:mm:ss")
    protected Date createTime;
    @JsonFormat(pattern="yyyy-MM-dd hh:mm:ss")
    protected Date updateTime;
}
