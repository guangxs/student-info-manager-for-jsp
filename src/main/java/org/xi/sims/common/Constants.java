package org.xi.sims.common;

/**
 * @author ：xi
 * @time ：Created in 22:30
 * @description：
 */
public class Constants {
    public static final String STUDENT = "student";
    public static final String TEACHER = "teacher";
    public static final String ADMIN = "admin";
    public static final String ERROR_SPLIT = "#";
}
