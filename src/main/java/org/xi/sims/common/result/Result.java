package org.xi.sims.common.result;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.xi.sims.util.XinUtil;

import java.io.Serializable;

/**
 * @author ：xi
 * @date ：Created in 18:26
 * @description：
 * @modified By：$
 * @version: $
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Result implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer code;
    private CharSequence message;
    private Object data;


    public static Result ok(){
        return result(ResultCode.SUCCESS, null, null, null);
    }

    public static Result check(boolean result){
        return result ? ok() : error("操作失败");
    }

    public static Result check(int result){
        return result > 0 ? ok() : error("操作失败");
    }

    public static Result ok(CharSequence msg){
        return result(ResultCode.SUCCESS, null, msg, null);
    }

    public Result ok(String msg, Object data){
        return result(ResultCode.SUCCESS, data, msg, null);
    }

    public static Result success(Object data){
        return result(ResultCode.SUCCESS, data, null, null);
    }


    public static Result success(Object data, CharSequence msg){
        return result(ResultCode.SUCCESS, data, msg, null);
    }
    public static Result error(CharSequence msg){
        return result(ResultCode.FAILURE, null, msg, null);
    }

    public static Result error(ResultCode resultCode){
        return result(resultCode, null, null, null);
    }

    public static Result error(ResultCode resultCode, CharSequence msg){
        return result(resultCode, null, msg, null);
    }

    public static Result error(int code, CharSequence msg){
        return result(ResultCode.FAILURE, null, msg, code);
    }

    private static Result result(ResultCode resultCode, Object data, CharSequence msg, Integer code) {
        Result result = new Result();
        result.code = XinUtil.selectNoNull(code, resultCode.getCode());
        result.message = XinUtil.selectNoNull(msg, resultCode.getDesc());
        result.data = XinUtil.selectNoNull(data);
        return result;
    }
}
