package org.xi.sims.common.result;

import lombok.Getter;
import lombok.Setter;

@Getter
public enum ResultCode {

    //成功
    SUCCESS( 200, "SUCCESS" ),
    //失败
    FAILURE( 400, "FAILURE" ),
    // 未登录
    UN_LOGIN( 401, "未登录" ),
    //未认证（签名错误、token错误）
    UNAUTHORIZED( 403, "无权操作" ),
    //未通过认证
    USER_UNAUTHORIZED( 402, "用户名或密码不正确" ),
    //接口不存在
    NOT_FOUND( 404, "资源不存在" ),
    //请求行中指定的请求方法不能被用于请求相应的资源。该响应必须返回一个Allow 头信息用以表示出当前资源能够接受的请求方法的列表。 　　
    // 鉴于 PUT，DELETE 方法会对服务器上的资源进行写操作，因而绝大部分的网页服务器都不支持或者在默认配置下不允许上述请求方法，对于此类请求均会返回405错误。
    NOT_ALOW(405, "不允许的请求方法"),
    //服务器内部错误
    INTERNAL_SERVER_ERROR( 500, "服务器内部错误" ),

    RFC(501, "数据异常");


    @Setter
    private int code;
    @Setter
    private String desc;

    ResultCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}

