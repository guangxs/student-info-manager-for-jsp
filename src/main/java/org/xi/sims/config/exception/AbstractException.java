package org.xi.sims.config.exception;

import org.springframework.http.HttpStatus;

public  abstract class AbstractException extends RuntimeException{

    public AbstractException(String message) {
        super(message);
    }

    public AbstractException(String message, HttpStatus status) {
        super(message);

    }

    public AbstractException(String message, Throwable cause) {
        super(message, cause);
    }
}
