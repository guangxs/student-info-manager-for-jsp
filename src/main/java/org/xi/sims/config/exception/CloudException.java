package org.xi.sims.config.exception;

import lombok.Getter;
import org.xi.sims.common.result.ResultCode;

public class CloudException extends AbstractException  {
    @Getter
    private int code;

    public CloudException(String message) {
        super(message);
    }

    public CloudException(ResultCode resultCode) {
        super(resultCode.getDesc());
        code = resultCode.getCode();
    }

    public CloudException(String message, ResultCode resultCode) {
        super(message);
        code = resultCode.getCode();
    }

    public CloudException(String message, Exception e) {
        this(message);
        e.printStackTrace();
    }

    public CloudException(Exception e) {
        super(e.getMessage());
        e.printStackTrace();
    }


    public CloudException(String message, Throwable cause) {
        super(message, cause);
    }
}
