package org.xi.sims.config.token;

import cn.dev33.satoken.stp.StpInterface;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.collection.ListUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.xi.sims.common.Constants;
import org.xi.sims.util.XinUtil;

import java.util.List;

/**
 * @author ：xi
 * @time ：Created in 10:38
 * @description：
 */
@Service
@AllArgsConstructor
public class StpInterfaceImpl implements StpInterface {


    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        return null;
    }

    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        List<String> list = ListUtil.list(false);
        if (XinUtil.equal(StpUtil.getLoginDevice(), Constants.ADMIN)) {
            list.add(Constants.ADMIN);
        }
        if (XinUtil.equal(StpUtil.getLoginDevice(), Constants.TEACHER)) {
            list.add(Constants.TEACHER);
        }
        if (XinUtil.equal(StpUtil.getLoginDevice(), Constants.STUDENT)) {
            list.add(Constants.STUDENT);
        }
        return list;
    }
}
