package org.xi.sims.controller;

import cn.dev33.satoken.annotation.SaCheckRole;
import org.springframework.web.bind.annotation.*;
import org.xi.sims.base.controller.IBaseController;
import org.xi.sims.common.Constants;
import org.xi.sims.common.result.Result;
import org.xi.sims.pojo.entity.Achievement;
import org.xi.sims.service.AchievementService;
import org.xi.sims.util.XinUtil;

import javax.validation.constraints.NotNull;

@CrossOrigin
@RestController
@RequestMapping("/api/achievenmet")
public class AchievementController extends IBaseController<Achievement, AchievementService> {
    @SaCheckRole(Constants.STUDENT)
    @GetMapping({"studentAchievement", "studentAchievement/{name}"})
    public Result studentAchievement(@PathVariable(value = "name", required = false) String name){
        if (XinUtil.isNull(name)) {
            return Result.success(service.findByStudentId(loginId()));
        }
        return Result.success(service.findByStudentId(loginId(), name));
    }

    @SaCheckRole(Constants.TEACHER)
    @GetMapping("teacherAchievement/{courseId}/{studentId}")
    public Result teacherAchievement(@PathVariable("courseId") Integer courseId,
                                     @PathVariable("studentId") Integer studentId){
        return Result.success(service.findByStudentIdAndCourseId(studentId, courseId));
    }

    @SaCheckRole(Constants.TEACHER)
    @PostMapping("saveAchievement")
    public Result saveAchievement(@RequestBody @NotNull Achievement achievement){
        return Result.success(service.saveOrUpdate(achievement));
    }
}
