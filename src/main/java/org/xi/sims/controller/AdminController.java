package org.xi.sims.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.xi.sims.base.controller.IBaseController;
import org.xi.sims.pojo.entity.Admin;
import org.xi.sims.service.AdminService;

/**
 * @author ：xi
 * @time ：Created in 21:47
 * @description：
 */
@CrossOrigin
@RestController
@RequestMapping("/api/admin")
public class AdminController extends IBaseController<Admin, AdminService> {
}
