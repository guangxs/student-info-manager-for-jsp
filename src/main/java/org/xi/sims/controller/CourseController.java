package org.xi.sims.controller;

import cn.dev33.satoken.annotation.SaCheckRole;
import org.springframework.web.bind.annotation.*;
import org.xi.sims.base.controller.IBaseController;
import org.xi.sims.common.Constants;
import org.xi.sims.common.result.Result;
import org.xi.sims.pojo.entity.Course;
import org.xi.sims.pojo.form.CourseForm;
import org.xi.sims.pojo.form.SearchCourseForm;
import org.xi.sims.service.CourseService;

@CrossOrigin
@RestController
@RequestMapping("/api/course")
public class CourseController extends IBaseController<Course, CourseService> {
    @SaCheckRole(Constants.ADMIN)
    @DeleteMapping("deleteCourse/{id}")
    public Result deleteRoom(@PathVariable("id") Integer id) {
        service.deleteCourse(id);
        return Result.ok();
    }

    @SaCheckRole(Constants.ADMIN)
    @PostMapping("saveOrUpdate")
    public Result saveOrUpdate(@RequestBody CourseForm form) {
        return Result.success(service.saveOrUpdate(form));
    }

    @SaCheckRole(Constants.ADMIN)
    @PostMapping("courses")
    public Result rooms(@RequestBody(required = false) SearchCourseForm form) {
        return Result.success(service.query(form));
    }

    @SaCheckRole(Constants.ADMIN)
    @GetMapping("select")
    public Result select() {
        return Result.success(service.select());
    }
}
