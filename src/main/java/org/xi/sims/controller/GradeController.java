package org.xi.sims.controller;

import cn.dev33.satoken.annotation.SaCheckRole;
import cn.dev33.satoken.annotation.SaMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.*;
import org.xi.sims.base.controller.IBaseController;
import org.xi.sims.common.Constants;
import org.xi.sims.common.result.Result;
import org.xi.sims.pojo.entity.Grade;
import org.xi.sims.pojo.form.EditGradeForm;
import org.xi.sims.pojo.form.GradeForm;
import org.xi.sims.pojo.form.SearchGradeForm;
import org.xi.sims.service.GradeService;
import org.xi.sims.service.StudentService;

import javax.validation.constraints.NotNull;

@CrossOrigin
@RestController
@RequestMapping("/api/grade")
public class GradeController extends IBaseController<Grade, GradeService> {

    @Autowired
    @Lazy
    private StudentService studentService;

    @SaCheckRole(Constants.ADMIN)
    @GetMapping("select")
    public Result select() {
        return Result.success(service.select());
    }

    @SaCheckRole(value = {Constants.TEACHER, Constants.ADMIN}, mode = SaMode.OR)
    @PostMapping("editNotice")
    public Result editNotice(@RequestBody @NotNull EditGradeForm form) {
        return Result.success(service.editNotice(form.getId(), form.getNotice()));
    }

    @SaCheckRole(Constants.STUDENT)
    @GetMapping("myGradeInfo")
    public Result myGradeInfo() {
        return Result.success(service.findVoById(
                studentService.findGradeIdById(loginId())
        ));
    }

    @SaCheckRole(Constants.ADMIN)
    @PostMapping("grades")
    public Result grades(@RequestBody(required = false) SearchGradeForm form) {
        return Result.success(service.query(form));
    }
    @SaCheckRole(Constants.ADMIN)
    @DeleteMapping("deleteGrade/{id}")
    public Result deleteGrade(@PathVariable("id") Integer id) {
        service.deleteGrade(id);
        return Result.ok();
    }
    @SaCheckRole(Constants.ADMIN)
    @PostMapping("saveOrUpdate")
    public Result saveOrUpdate(@RequestBody GradeForm form) {
        return Result.success(service.saveOrUpdate(form));
    }
}
