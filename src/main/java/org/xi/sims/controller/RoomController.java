package org.xi.sims.controller;

import cn.dev33.satoken.annotation.SaCheckRole;
import org.springframework.web.bind.annotation.*;
import org.xi.sims.base.controller.IBaseController;
import org.xi.sims.common.Constants;
import org.xi.sims.common.result.Result;
import org.xi.sims.pojo.entity.Room;
import org.xi.sims.pojo.form.RoomForm;
import org.xi.sims.pojo.form.SearchRoomForm;
import org.xi.sims.service.RoomService;

@CrossOrigin
@RestController
@RequestMapping("/api/room")
public class RoomController extends IBaseController<Room, RoomService> {
    @SaCheckRole(Constants.ADMIN)
    @PostMapping("rooms")
    public Result rooms(@RequestBody(required = false) SearchRoomForm form) {
        return Result.success(service.query(form));
    }

    @SaCheckRole(Constants.ADMIN)
    @GetMapping("select")
    public Result select() {
        return Result.success(service.select());
    }
    @SaCheckRole(Constants.ADMIN)
    @DeleteMapping("deleteRoom/{id}")
    public Result deleteRoom(@PathVariable("id") Integer id) {
        service.deleteRoom(id);
        return Result.ok();
    }

    @SaCheckRole(Constants.ADMIN)
    @PostMapping("saveOrUpdate")
    public Result saveOrUpdate(@RequestBody RoomForm form) {
        return Result.success(service.saveOrUpdate(form));
    }
}
