package org.xi.sims.controller;

import cn.dev33.satoken.annotation.SaCheckRole;
import cn.dev33.satoken.annotation.SaMode;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.xi.sims.base.controller.IBaseController;
import org.xi.sims.common.Constants;
import org.xi.sims.common.result.Result;
import org.xi.sims.pojo.entity.Student;
import org.xi.sims.pojo.form.SearchStudentForm;
import org.xi.sims.pojo.form.StudentForm;
import org.xi.sims.service.StudentService;

import javax.validation.constraints.NotNull;

@CrossOrigin
@RestController
@RequestMapping("/api/student")
public class StudentController extends IBaseController<Student, StudentService> {


    @SaCheckRole(Constants.STUDENT)
    @GetMapping("myInfo")
    public Result myInfo() {
        return Result.success(service.vo(service.findById(loginId())));
    }

    @SaCheckRole(value = {Constants.STUDENT, Constants.TEACHER}, mode = SaMode.OR)
    @GetMapping("info/{id}")
    public Result info(@PathVariable(value = "id") Integer id) {
        return Result.success(service.vo(service.findById(id)));
    }

    @SaCheckRole(value = {Constants.ADMIN, Constants.TEACHER}, mode = SaMode.OR)
    @PostMapping("saveOrUpdate")
    public Result saveOrUpdate(@RequestBody @Validated @NotNull StudentForm form) {
        return Result.success(service.saveOrUpdate(form));
    }

    @SaCheckRole(Constants.STUDENT)
    @PostMapping("search")
    public Result search(@RequestBody @NotNull SearchStudentForm form) {
        return Result.success(service.query(form));
    }

    @SaCheckRole(Constants.STUDENT)
    @PostMapping("editMyInfo")
    public Result editMyInfo(@RequestBody @NotNull StudentForm form) {
        form.setId(loginId());
        return Result.success(service.edit(form));
    }
}
