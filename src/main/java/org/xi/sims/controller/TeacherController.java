package org.xi.sims.controller;

import cn.dev33.satoken.annotation.SaCheckRole;
import cn.dev33.satoken.annotation.SaMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.*;
import org.xi.sims.base.controller.IBaseController;
import org.xi.sims.common.Constants;
import org.xi.sims.common.result.Result;
import org.xi.sims.pojo.entity.Teacher;
import org.xi.sims.pojo.form.SearchTeacherForm;
import org.xi.sims.pojo.form.TeacherForm;
import org.xi.sims.service.GradeService;
import org.xi.sims.service.TeacherService;

@CrossOrigin
@RestController
@RequestMapping("/api/teacher")
public class TeacherController extends IBaseController<Teacher, TeacherService> {
    @Autowired
    @Lazy
    private GradeService gradeService;

    @SaCheckRole(Constants.TEACHER)
    @GetMapping("myInfo")
    public Result myInfo() {
        return Result.success(service.vo(service.findById(loginId())));
    }


    @SaCheckRole(Constants.TEACHER)
    @PostMapping("editMyInfo")
    public Result editMyInfo(@RequestBody TeacherForm form) {
        form.setId(loginId());
        return Result.success(service.edit(form));
    }

    @SaCheckRole(value = {Constants.TEACHER, Constants.ADMIN}, mode = SaMode.OR)
    @PostMapping("grades")
    public Result grades() {
        return Result.success(gradeService.findVOByTeacherId(loginId()));
    }

    @SaCheckRole(Constants.ADMIN)
    @PostMapping("teachers")
    public Result teachers(@RequestBody(required = false) SearchTeacherForm form) {
        return Result.success(service.query(form));
    }

    @SaCheckRole(Constants.ADMIN)
    @GetMapping("select")
    public Result select() {
        return Result.success(service.select());
    }
    @SaCheckRole(Constants.ADMIN)
    @PostMapping("saveOrUpdate")
    public Result saveOrUpdate(@RequestBody TeacherForm form) {
        return Result.success(service.saveOrUpdate(form));
    }

    @SaCheckRole(Constants.ADMIN)
    @DeleteMapping("deleteTeacher/{id}")
    public Result deleteTeacher(@PathVariable("id") Integer id) {
        service.deleteTeacher(id);
        return Result.ok();
    }
}
