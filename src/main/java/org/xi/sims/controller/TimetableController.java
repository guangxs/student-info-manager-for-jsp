package org.xi.sims.controller;

import cn.dev33.satoken.annotation.SaCheckRole;
import org.springframework.web.bind.annotation.*;
import org.xi.sims.base.controller.IBaseController;
import org.xi.sims.common.Constants;
import org.xi.sims.common.result.Result;
import org.xi.sims.pojo.entity.Timetable;
import org.xi.sims.pojo.form.SearchTimetableForm;
import org.xi.sims.pojo.form.TimetableForm;
import org.xi.sims.service.TimetableService;
import org.xi.sims.util.XinUtil;

@CrossOrigin
@RestController
@RequestMapping("/api/timetable")
public class TimetableController extends IBaseController<Timetable, TimetableService> {

    @SaCheckRole(Constants.STUDENT)
    @GetMapping({"studentTimetable", "studentTimetable/{name}"})
    public Result studentTimetable(@PathVariable(value = "name", required = false) String name){
        if (XinUtil.isNull(name)) {
            return Result.success(service.findVoByStudentId(loginId()));
        }
        return Result.success(service.findVoByStudentIdAndName(loginId(), name));
    }

    @SaCheckRole(Constants.TEACHER)
    @GetMapping("teacherTimetable")
    public Result teacherTimetable(){
        return Result.success(service.findVosByTeacherId(loginId()));
    }

    @SaCheckRole(Constants.ADMIN)
    @PostMapping("saveOrUpdate")
    public Result saveOrUpdate(@RequestBody TimetableForm form) {
        return Result.success(service.saveOrUpdate(form));
    }

    @SaCheckRole(Constants.ADMIN)
    @PostMapping("timetables")
    public Result rooms(@RequestBody(required = false) SearchTimetableForm form) {
        return Result.success(service.query(form));
    }
}
