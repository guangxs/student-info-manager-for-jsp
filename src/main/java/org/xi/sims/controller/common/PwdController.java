package org.xi.sims.controller.common;

import cn.dev33.satoken.annotation.SaCheckRole;
import cn.dev33.satoken.annotation.SaMode;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.bean.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.xi.sims.base.controller.IAbstractController;
import org.xi.sims.common.Constants;
import org.xi.sims.common.result.Result;
import org.xi.sims.common.result.ResultCode;
import org.xi.sims.config.exception.CloudException;
import org.xi.sims.pojo.entity.Admin;
import org.xi.sims.pojo.entity.Student;
import org.xi.sims.pojo.entity.Teacher;
import org.xi.sims.pojo.form.EditPwdForm;
import org.xi.sims.service.AdminService;
import org.xi.sims.service.StudentService;
import org.xi.sims.service.TeacherService;
import org.xi.sims.util.IAssert;
import org.xi.sims.util.XinUtil;

import javax.validation.constraints.NotNull;

/**
 * @author ：xi
 * @time ：Created in 21:50
 * @description：
 */
@CrossOrigin
@RestController
@RequestMapping("/api/editPwd")
public class PwdController extends IAbstractController {
    @Autowired
    @Lazy
    private StudentService studentService;
    @Autowired
    @Lazy
    private TeacherService teacherService;
    @Autowired
    @Lazy
    private AdminService adminService;

    @SaCheckRole(value = {Constants.ADMIN, Constants.TEACHER, Constants.STUDENT}, mode = SaMode.OR)
    @PostMapping
    public Result login(@RequestBody @Validated @NotNull EditPwdForm form) {
        if (XinUtil.noEqual(form.getNewPassword(), form.getConfirmNewPassword())) {
            throw new CloudException("两次密码不一致");
        }
        String pwd = form.getOriginalPassword();
        String newPwd = form.getNewPassword();

        if (StpUtil.hasRole(Constants.ADMIN)) {
            editAdmin(pwd, newPwd);
        } else if (StpUtil.hasRole(Constants.TEACHER)) {
            editTeacher(pwd, newPwd);
        } else if (StpUtil.hasRole(Constants.STUDENT)) {
            editStudent(pwd, newPwd);
        }
        return Result.ok();
    }



    private Admin editAdmin(String pwd, String newPwd) {
        Admin e = adminService.findById(loginId());
        eq(e, pwd).setPassword(XinUtil.Md5(newPwd));
        return adminService.save(e);
    }

    private Teacher editTeacher(String pwd, String newPwd) {
        Teacher e = teacherService.findById(loginId());
        eq(e, pwd).setPassword(XinUtil.Md5(newPwd));
        return teacherService.save(e);
    }

    private Student editStudent(String pwd, String newPwd) {
        Student e = studentService.findById(loginId());
        eq(e, pwd).setPassword(XinUtil.Md5(newPwd));
        return studentService.save(e);
    }

    private <E> E eq(E e, String pwd) {
        if (XinUtil.noEqual(
                XinUtil.Md5(pwd),
                BeanUtil.getFieldValue(
                        IAssert.isNull(e, ResultCode.USER_UNAUTHORIZED),
                        "password")
        )) {
            throw new CloudException(ResultCode.USER_UNAUTHORIZED);
        }
        return e;
    }
}
