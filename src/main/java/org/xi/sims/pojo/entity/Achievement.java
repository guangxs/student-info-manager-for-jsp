package org.xi.sims.pojo.entity;

import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.xi.sims.base.pojo.entity.IBaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author ：xi
 */
@ToString
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "sims_achievement")
@EntityListeners(AuditingEntityListener.class)
public class Achievement extends IBaseEntity<Achievement> implements Serializable {
    private static final long serialVersionUID = 1L;

    //学生ID
    @Column
    private Integer studentId;

    //课程号
    @Column
    private Integer courseId;

    //分数
    @Column
    private Double score;
}
