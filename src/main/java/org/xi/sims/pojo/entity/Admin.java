package org.xi.sims.pojo.entity;

import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.xi.sims.base.pojo.entity.IBaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author ：xi
 * @time ：Created in 17:47
 * @description：
 */
@ToString
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "sims_admin")
@EntityListeners(AuditingEntityListener.class)
public class Admin extends IBaseEntity<Admin> implements Serializable {
    private static final long serialVersionUID = 1L;

    //工号
    @Column(length = 32)
    private String number;

    //密码
    @Column(length = 64)
    private String password;

    //电话
    @Column(length = 16)
    private String phone;
}
