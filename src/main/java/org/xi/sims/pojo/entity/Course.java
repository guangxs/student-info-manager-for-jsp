package org.xi.sims.pojo.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.xi.sims.base.pojo.entity.IBaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author ：xi
 */
@ToString
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "sims_course")
@EntityListeners(AuditingEntityListener.class)
public class Course extends IBaseEntity<Course> implements Serializable {
    private static final long serialVersionUID = 1L;

    //课程号
    @Column(length = 16)
    private String number;

    //课程名
    @Column(length = 32)
    private String name;

    //学时
    @Column
    private Integer learnTime;

    //学分
    @Column
    private Integer credit;
}
