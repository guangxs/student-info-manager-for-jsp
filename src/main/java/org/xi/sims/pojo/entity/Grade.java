package org.xi.sims.pojo.entity;

import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.xi.sims.base.pojo.entity.IBaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author ：xi
 */
@ToString
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "sims_grade")
@EntityListeners(AuditingEntityListener.class)
public class Grade extends IBaseEntity<Grade> implements Serializable {
    private static final long serialVersionUID = 1L;

    //班级名
    @Column(length = 32)
    private String name;

    //班级号
    @Column(length = 16)
    private String number;

    //班主任ID
    @Column
    private Integer teacherId;

    //班级公告
    @Column(length = 512)
    private String notice;
}

