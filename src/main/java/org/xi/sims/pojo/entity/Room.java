package org.xi.sims.pojo.entity;

import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.xi.sims.base.pojo.entity.IBaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author ：xi
 */
@ToString
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "sims_room")
@EntityListeners(AuditingEntityListener.class)
public class Room extends IBaseEntity<Room> implements Serializable {
    private static final long serialVersionUID = 1L;

    //教室所在楼
    @Column(length = 16)
    private String level;

    //教室(
    @Column
    private Integer room;

    //教室号
    @Column(length = 32)
    private String number;
}
