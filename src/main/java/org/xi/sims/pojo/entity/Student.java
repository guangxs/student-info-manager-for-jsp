package org.xi.sims.pojo.entity;
//承载数据
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.xi.sims.base.pojo.entity.IBaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author ：xi
 */
@ToString
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@Entity//实体
@Table(name = "sims_student")//映射表
@EntityListeners(AuditingEntityListener.class)
public class Student extends IBaseEntity<Student> implements Serializable {
	//extends继承类IBaseEntity（基础类）Serializable接口实现序列化（序列话将对象的状态信息转换为可以存储或传输的形式的过程
    private static final long serialVersionUID = 1L;

    //学号
    @Column(length = 32)
    private String number;

    //班级ID'
    @Column
    private Integer gradeId;

    //职位
    @Column(length = 32)
    private String position;
    
    //姓名
    @Column(length = 64)
    private String name;

    //昵称
    @Column(length = 32)
    private String nickname;

    //性别
    @Column(length = 2)
    private String sex;
    
    //专业
    @Column(length = 16)
    private String major;

    //头像
    @Column
    private String avatar;

    //密码
    @JsonIgnore
    @Column(length = 64)
    private String password;

    //邮箱
    @Column(length = 64)
    private String email;

    //电话
    @Column(length = 16)
    private String phone;

    //住址
    @Column
    private String address;

    //出生日期
    @JsonFormat(pattern="yyyy-MM-dd hh:mm:ss")
    private Date birth;


    
}

