package org.xi.sims.pojo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.xi.sims.base.pojo.entity.IBaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author ：xi
 */
@ToString
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "sims_teacher")
@EntityListeners(AuditingEntityListener.class)
public class Teacher extends IBaseEntity<Teacher> implements Serializable {
    private static final long serialVersionUID = 1L;

    //工号
    @Column(length = 32)
    private String number;

    //职称
    @Column(length = 16)
    private String leader;
    
    //姓名
    @Column(length = 64)
    private String name;

    //性别
    @Column(length = 2)
    private String sex;

    //头像
    @Column
    private String avatar;

    //密码
    @Column(length = 64)
    private String password;

    //电话
    @Column(length = 16)
    private String phone;

    //住址
    @Column
    private String address;

    //出生日期
    @JsonFormat(pattern="yyyy-MM-dd hh:mm:ss")
    private Date birth;
}
