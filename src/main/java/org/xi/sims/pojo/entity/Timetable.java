package org.xi.sims.pojo.entity;


import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.xi.sims.base.pojo.entity.IBaseEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author ：xi
 */
@ToString
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "sims_timetable")
@EntityListeners(AuditingEntityListener.class)
public class Timetable extends IBaseEntity<Timetable> implements Serializable {
    private static final long serialVersionUID = 1L;

    //班级ID
    @Column
    private Integer gradeId;

    //课程ID
    @Column
    private Integer courseId;

    //授课教室
    @Column
    private Integer roomId;

    //授课起始周
    @Column
    private Integer start;

    //授课结束周
    @Column
    private Integer end;

    //授课周
    @Column
    private Integer week;

    //授课节
    @Column
    private Integer knob;

    //授课教师
    @Column
    private Integer teacherId;
}
