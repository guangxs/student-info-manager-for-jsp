package org.xi.sims.pojo.form;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * @author ：xi
 * @time ：Created in 10:45
 * @description：
 */
@Data
/*忽略未知参数*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class CourseForm {
    private Integer id;
    private String number;
    private String name;
    private Integer learnTime;

    //学分
    private Integer credit;
}
