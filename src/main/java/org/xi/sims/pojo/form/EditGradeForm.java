package org.xi.sims.pojo.form;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * @author ：xi
 * @time ：Created in 20:56
 * @description：
 */
@Data
/*忽略未知参数*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class EditGradeForm {
    private Integer id;

    private String notice;
    //班主任ID
    private Integer teacherId;
    //班级号
    private String number;
    //班级名
    private String name;
}
