package org.xi.sims.pojo.form;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author ：xi
 * @time ：Created in 21:48
 * @description：
 */
@Data
/*忽略未知参数*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class EditPwdForm {
    @NotBlank(message = "原密码不能为空")
    @Length(max = 16, min = 1, message = "密码长度应在1-16个字符之间")
    private String originalPassword;
    @NotBlank(message = "新密码不能为空")
    @Length(max = 16, min = 1, message = "密码长度应在1-16个字符之间")
    private String newPassword;
    @NotBlank(message = "确认新密码不能为空")
    @Length(max = 16, min = 1, message = "密码长度应在1-16个字符之间")
    private String confirmNewPassword;
}
