package org.xi.sims.pojo.form;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ：xi
 * @time ：Created in 17:20
 * @description：
 */
@Data
/*忽略未知参数*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class GradeForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String name;
    private String number;
    private Integer teacherId;
}
