package org.xi.sims.pojo.form;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Data
/*忽略未知参数*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginForm implements Serializable {
    private static final long serialVersionUID = 1L;
    
	@NotBlank(message = "账号不能为空")
    @Pattern(regexp = "^[\\u4E00-\\u9FA5A-Za-z0-9_]+$", message = "账号格式应包含中文、英文、数字、下划线")
    @Length(max = 16, min = 1, message = "账号长度应在1-16个字符之间")
    private String number;

   /* @NotBlank(message = "邮箱不能为空")
    @Email(message = "邮箱格式有误")
    private String email;*/

    @NotBlank(message = "密码不能为空")
    @Length(max = 16, min = 1, message = "密码长度应在1-16个字符之间")
    private String password;

    private int type;
}
