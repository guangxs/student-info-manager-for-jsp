package org.xi.sims.pojo.form;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author ：xi
 * @time ：Created in 17:57
 * @description：
 */
@Data
/*忽略未知参数*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class RoomForm {
    private Integer id;
    //教室所在楼
    @NotBlank(message = "教学楼不能为空")
    private String level;

    //教室(
    @NotBlank(message = "教室不能为空")
    private Integer room;

    //教室号
    @NotBlank(message = "教室号不能为空")
    private String number;
}
