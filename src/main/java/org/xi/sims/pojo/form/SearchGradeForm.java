package org.xi.sims.pojo.form;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * @author ：xi
 * @time ：Created in 17:20
 * @description：
 */
@Data
/*忽略未知参数*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchGradeForm {
    private String name;
    private String number;
}
