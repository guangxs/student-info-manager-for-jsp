package org.xi.sims.pojo.form;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * @author ：xi
 * @time ：Created in 17:55
 * @description：
 */
@Data
/*忽略未知参数*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchRoomForm {
    private String level;
    private Integer room;
    private String number;
}
