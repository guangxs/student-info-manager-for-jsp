package org.xi.sims.pojo.form;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * @author ：xi
 * @time ：Created in 22:51
 * @description：
 */
@Data
/*忽略未知参数*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchStudentForm {
    private String number;
    //所在班级ID'
    private Integer gradeId;
    //所在班级名称
    private String gradeName;

    //职位
    private String position;

    //姓名
    private String name;

    //昵称
    private String nickname;
    
  //专业
    private String major;

    //性别
    private String sex;

    //头像
    private String avatar;

    //邮箱
    private String email;

    //电话
    private String phone;

    //住址
    private String address;
}
