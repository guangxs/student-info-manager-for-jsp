package org.xi.sims.pojo.form;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * @author ：xi
 * @time ：Created in 22:51
 * @description：
 */
@Data
/*忽略未知参数*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchTeacherForm {
    private String number;
    private String leader;

    //姓名
    private String name;

    //性别
    private String sex;

    //头像
    private String avatar;

    //电话
    private String phone;

    //住址
    private String address;
}
