package org.xi.sims.pojo.form;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;

/**
 * @author ：xi
 * @time ：Created in 13:59
 * @description：
 */
@Data
/*忽略未知参数*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class StudentForm implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;

    @NotBlank(message = "学号不能为空")
    @Pattern(regexp = "^[\\u4E00-\\u9FA5A-Za-z0-9_]+$", message = "学号格式应包含中文、英文、数字、下划线")
    @Length(max = 16, min = 1, message = "学号长度应在1-16个字符之间")
    private String number;

    //所在班级ID'
    private Integer gradeId;

    //职位
    private String position;

    //姓名
    private String name;
    
  //专业
    private String major;

    //昵称
    @NotBlank(message = "昵称不能为空")
    @Pattern(regexp = "^[\\u4E00-\\u9FA5A-Za-z0-9_]+$", message = "昵称格式应包含中文、英文、数字、下划线")
    @Length(max = 16, min = 1, message = "学号长度应在1-16个字符之间")
    private String nickname;

    //性别
    private String sex;

    //邮箱
    @Email(message = "邮箱格式有误")
    private String email;

    //电话
    private String phone;

    //住址
    private String address;

    //出生日期
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date birth;
}
