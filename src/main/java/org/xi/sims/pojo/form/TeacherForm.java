package org.xi.sims.pojo.form;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author ：xi
 * @time ：Created in 12:29
 * @description：
 */
@Data
/*忽略未知参数*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class TeacherForm implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;

    //姓名
    private String name;
    private String leader;
    private String number;
    //性别
    private String sex;
    //电话
    private String phone;

    //住址
    private String address;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date birth;
}
