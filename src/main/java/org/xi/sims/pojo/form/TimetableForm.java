package org.xi.sims.pojo.form;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * @author ：xi
 * @time ：Created in 11:16
 * @description：
 */
@Data
/*忽略未知参数*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class TimetableForm {
    private Integer id;
    //班级ID
    private Integer gradeId;

    //课程ID
    private Integer courseId;

    //授课教室
    private Integer roomId;

    //授课起始周
    private Integer start;

    //授课结束周
    private Integer end;

    //授课周
    private Integer week;

    //授课节
    private Integer knob;

    //授课教师
    private Integer teacherId;
}
