package org.xi.sims.pojo.vo;

import lombok.*;
import org.xi.sims.base.vo.IBaseVO;

import java.io.Serializable;

/**
 * @author ：xi
 * @time ：Created in 19:07
 * @description：
 */
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class AchievementVO extends IBaseVO<AchievementVO> implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    //学生ID
    private Integer studentId;

    private StudentVO student;

    //课程号
    private Integer courseId;

    private CourseVO course;

    //分数
    private Double score;
}
