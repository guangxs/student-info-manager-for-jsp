package org.xi.sims.pojo.vo;

import lombok.*;
import org.xi.sims.base.vo.IBaseVO;

import java.io.Serializable;

/**
 * @author ：xi
 * @time ：Created in 19:03
 * @description：
 */
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CourseVO extends IBaseVO<CourseVO> implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    //课程号
    private String number;

    //课程名
    private String name;

    //学时
    private Integer learnTime;

    //学分
    private Integer credit;
}
