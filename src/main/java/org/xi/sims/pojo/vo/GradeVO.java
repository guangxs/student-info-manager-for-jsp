package org.xi.sims.pojo.vo;

import cn.hutool.core.collection.ListUtil;
import lombok.*;
import org.xi.sims.base.vo.IBaseVO;

import java.io.Serializable;
import java.util.List;

/**
 * @author ：xi
 * @time ：Created in 18:51
 * @description：
 */
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class GradeVO extends IBaseVO<GradeVO> implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;

    //班级名
    private String name;

    //班级号
    private String number;

    //班主任ID
    private Integer teacherId;

    //班级公告
    private String notice;

    private TeacherVO teacher;

    //班级学生
    private List<StudentVO> students = ListUtil.list(false);

    //班级课程
    private List<CourseVO> courses = ListUtil.list(false);
}
