package org.xi.sims.pojo.vo;

import lombok.*;
import org.xi.sims.base.vo.IBaseVO;

import java.io.Serializable;

/**
 * @author ：xi
 * @time ：Created in 19:05
 * @description：
 */
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class RoomVO extends IBaseVO<RoomVO> implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    //教室所在楼
    private String level;

    //教室(
    private Integer room;

    //教室号
    private String number;

    //是否被占用
    private Boolean isEmploy;
}
