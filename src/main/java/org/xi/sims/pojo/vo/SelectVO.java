package org.xi.sims.pojo.vo;

import lombok.*;

/**
 * @author ：xi
 * @time ：Created in 11:04
 * @description：
 */
@ToString
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SelectVO {
    private Integer value;
    private String label;
}
