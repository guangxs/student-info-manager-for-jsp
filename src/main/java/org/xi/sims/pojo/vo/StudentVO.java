package org.xi.sims.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.xi.sims.base.vo.IBaseVO;
import org.xi.sims.util.CreateNamePicture;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

/**
 * @author ：xi
 * @time ：Created in 18:46
 * @description：
 */
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class StudentVO extends IBaseVO<StudentVO> implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;

    private String number;

    //所在班级ID'
    private Integer gradeId;

    //所在班级名称
    private String gradeName;
    
  //专业
    private String major;

    //职位
    private String position;

    //姓名
    private String name;

    //昵称
    private String nickname;

    //性别
    private String sex;

    //头像
    private String avatar;

    public void setAvatar(String avatar) {
        try {
            this.avatar = CreateNamePicture.generateImg(name);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //邮箱
    private String email;

    //电话
    private String phone;

    //住址
    private String address;

    //出生日期
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date birth;

}
