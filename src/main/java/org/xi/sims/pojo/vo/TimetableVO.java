package org.xi.sims.pojo.vo;

import lombok.*;
import org.xi.sims.base.vo.IBaseVO;

import java.io.Serializable;

/**
 * @author ：xi
 * @time ：Created in 19:01
 * @description：
 */
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class TimetableVO extends IBaseVO<TimetableVO> implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    //班级ID
    private Integer gradeId;

    private GradeVO grade;

    //课程ID
    private Integer courseId;

    private CourseVO course;

    //授课教室
    private Integer roomId;

    private RoomVO room;

    //授课起始周
    private Integer start;

    //授课结束周
    private Integer end;

    //授课周
    private Integer week;

    //授课节
    private Integer knob;

    //授课教师
    private Integer teacherId;

    private TeacherVO teacher;
}
