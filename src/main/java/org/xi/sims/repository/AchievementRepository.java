package org.xi.sims.repository;


import org.springframework.stereotype.Repository;
import org.xi.sims.base.repository.IBaseRepository;
import org.xi.sims.pojo.entity.Achievement;

import java.util.List;

@Repository
public interface AchievementRepository extends IBaseRepository<Achievement> {
    List<Achievement> findByScoreBetween(int max, int min);

    List<Achievement> findByStudentId(int studentId);

    List<Achievement> findByCourseId(int CourseId);

    Achievement findByStudentIdAndCourseId(int studentId, int courseId);
}
