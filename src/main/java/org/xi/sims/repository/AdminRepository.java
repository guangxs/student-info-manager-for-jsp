package org.xi.sims.repository;

import org.springframework.stereotype.Repository;
import org.xi.sims.base.repository.IBaseRepository;
import org.xi.sims.pojo.entity.Admin;

/**
 * @author ：xi
 * @time ：Created in 21:28
 * @description：
 */
@Repository
public interface AdminRepository extends IBaseRepository<Admin> {
    Admin findByNumber(String number);
}
