package org.xi.sims.repository;


import org.springframework.stereotype.Repository;
import org.xi.sims.base.repository.IBaseRepository;
import org.xi.sims.pojo.entity.Course;

import java.util.List;

@Repository
public interface CourseRepository extends IBaseRepository<Course> {

	Course findByNumber(String number);

	List<Course> findByNameLikeOrderByLearnTimeAsc(String name);

	List<Course> findByNameLikeOrderByCreditAsc(String name);
}
