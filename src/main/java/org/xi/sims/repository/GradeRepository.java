package org.xi.sims.repository;


import org.springframework.stereotype.Repository;
import org.xi.sims.base.repository.IBaseRepository;
import org.xi.sims.pojo.entity.Grade;

import java.util.List;

@Repository
public interface GradeRepository extends IBaseRepository<Grade> {
	Grade findByNumber(String number);

	List<Grade> findByTeacherId(int teacherId);
}
