package org.xi.sims.repository;


import org.springframework.stereotype.Repository;
import org.xi.sims.base.repository.IBaseRepository;
import org.xi.sims.pojo.entity.Room;

@Repository
public interface RoomRepository extends IBaseRepository<Room> {
    Room findByNumber(String number);
}
