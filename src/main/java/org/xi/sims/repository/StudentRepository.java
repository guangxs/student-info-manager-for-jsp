package org.xi.sims.repository;


import org.springframework.stereotype.Repository;
import org.xi.sims.base.repository.IBaseRepository;
import org.xi.sims.pojo.entity.Student;

import java.util.List;

@Repository
public interface StudentRepository extends IBaseRepository<Student> {//interface是接口StudentRepository从数据库拿数据
	
	List<Student> findByGradeId(Integer gradeId);
	//根据姓名查找
	Student findByName(String name);
	
	Student findByNumber(String number);
}
