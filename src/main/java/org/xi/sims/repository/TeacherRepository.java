package org.xi.sims.repository;


import org.springframework.stereotype.Repository;
import org.xi.sims.base.repository.IBaseRepository;
import org.xi.sims.pojo.entity.Teacher;

import java.util.List;

@Repository
public interface TeacherRepository extends IBaseRepository<Teacher> {


	Teacher findByNumberAndPassword(String number, String password);

	List<Teacher> findBySex(String sex);

	List<Teacher> findByLeader(String leader);

	Teacher findByNumber(String number);
}
