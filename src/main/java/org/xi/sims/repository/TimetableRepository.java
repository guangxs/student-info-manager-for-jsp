package org.xi.sims.repository;


import org.springframework.stereotype.Repository;
import org.xi.sims.base.repository.IBaseRepository;
import org.xi.sims.pojo.entity.Timetable;

import java.util.List;

@Repository
public interface TimetableRepository extends IBaseRepository<Timetable> {

	List<Timetable> findByTeacherId(Integer teacherId);
	
	List<Timetable> findByCourseId(Integer courseId);

	List<Timetable> findByGradeId(Integer gradeId);

	List<Timetable> findByRoomId(Integer roomId);
}
