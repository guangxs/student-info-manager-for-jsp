package org.xi.sims.router;

import cn.dev33.satoken.annotation.SaCheckRole;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.xi.sims.common.Constants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author ：xi
 * @time ：Created in 15:22
 * @description：
 */
@Controller
@RequestMapping("/admin")
public class AdminRouter {
    @SaCheckRole(Constants.ADMIN)
    @GetMapping("timetable")
    public ModelAndView timetable(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("/admin/timetable")
                .addObject("title", "课程安排");
    }
    @SaCheckRole(Constants.ADMIN)
    @GetMapping("courses")
    public ModelAndView courses(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("/admin/courses")
                .addObject("title", "课程管理");
    }
    @SaCheckRole(Constants.ADMIN)
    @GetMapping("teachers")
    public ModelAndView teachers(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("/admin/teachers")
                .addObject("title", "教师管理");
    }
    @SaCheckRole(Constants.ADMIN)
    @GetMapping("grades")
    public ModelAndView grades(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("/admin/grades")
                .addObject("title", "班级管理");
    }
    @SaCheckRole(Constants.ADMIN)
    @GetMapping("rooms")
    public ModelAndView rooms(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("/admin/rooms")
                .addObject("title", "教室管理");
    }
    @SaCheckRole(Constants.ADMIN)
    @GetMapping("setting")
    public ModelAndView setting(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("/admin/setting")
                .addObject("title", "设 置");
    }
}
