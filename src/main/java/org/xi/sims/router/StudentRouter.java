package org.xi.sims.router;

import cn.dev33.satoken.annotation.SaCheckRole;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.xi.sims.base.controller.IAbstractController;
import org.xi.sims.common.Constants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author ：xi
 * @time ：Created in 10:47
 * @description：
 */
@Controller
@RequestMapping("/student")
public class StudentRouter extends IAbstractController {

    @SaCheckRole(Constants.STUDENT)
    @GetMapping("info")
    public ModelAndView info(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("/student/info")
                .addObject("title", "我的信息");
    }

    @SaCheckRole(Constants.STUDENT)
    @GetMapping("grade")
    public ModelAndView grade(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("/student/grade")
                .addObject("title", "我的班级");
    }

    @SaCheckRole(Constants.STUDENT)
    @GetMapping("course")
    public ModelAndView course(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("/student/course")
                .addObject("title", "我的课程");
    }

    @SaCheckRole(Constants.STUDENT)
    @GetMapping("edit")
    public ModelAndView edit(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("/student/edit")
                .addObject("id", loginId())
                .addObject("title", "修改信息");
    }

    @SaCheckRole(Constants.STUDENT)
    @GetMapping("score")
    public ModelAndView search(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("/student/score")
                .addObject("id", loginId())
                .addObject("title", "成绩查询");
    }
}
