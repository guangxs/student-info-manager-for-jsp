package org.xi.sims.router;

import cn.dev33.satoken.annotation.SaCheckRole;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.xi.sims.base.controller.IAbstractController;
import org.xi.sims.common.Constants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author ：xi
 * @time ：Created in 22:57
 * @description：
 */
@Controller
@RequestMapping("/teacher")
public class TeacherRouter extends IAbstractController {
    @SaCheckRole(Constants.TEACHER)
    @GetMapping("info")
    public ModelAndView info(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("/teacher/info")
                .addObject("title", "个人中心");
    }

    @SaCheckRole(Constants.TEACHER)
    @GetMapping("grades")
    public ModelAndView students(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("/teacher/grades")
                .addObject("title", "班级管理");
    }
    @SaCheckRole(Constants.TEACHER)
    @GetMapping("timetable")
    public ModelAndView timetable(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("/teacher/timetable")
                .addObject("title", "我的教学");
    }
    @SaCheckRole(Constants.TEACHER)
    @GetMapping("setting")
    public ModelAndView setting(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("/teacher/setting")
                .addObject("title", "设 置");
    }
}
