package org.xi.sims.router.common;

import cn.dev33.satoken.stp.StpUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author ：xi
 * @time ：Created in 23:35
 * @description：
 */
@Controller
@RequestMapping("/")
public class CommonRouter {

    @GetMapping("login")
    public ModelAndView toLogin(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("/login");
    }

    @GetMapping("logout")
    public ModelAndView logout(HttpServletRequest request, HttpServletResponse response) {
        StpUtil.logout();
        return new ModelAndView("/login");
    }
}
