package org.xi.sims.service;

import org.xi.sims.base.service.IBaseService;
import org.xi.sims.pojo.entity.Achievement;
import org.xi.sims.pojo.vo.AchievementVO;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author ：xi
 * @time ：Created in 19:09
 * @description：
 */
public interface AchievementService extends IBaseService<Achievement> {
    List<AchievementVO> findByStudentId(@NotNull Integer studentId, String courseName);

    List<AchievementVO> findByStudentId(@NotNull Integer studentId);

    List<AchievementVO> vos(List<Achievement> achievements);

    AchievementVO vo(@NotNull Achievement achievement);

    AchievementVO findByStudentIdAndCourseId(Integer studentId, Integer courseId);

    Achievement saveOrUpdate(@NotNull Achievement achievement);
}
