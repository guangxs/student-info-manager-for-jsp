package org.xi.sims.service;

import org.xi.sims.base.service.IBaseService;
import org.xi.sims.pojo.entity.Admin;

import javax.validation.constraints.NotNull;

/**
 * @author ：xi
 * @time ：Created in 19:09
 * @description：
 */
public interface AdminService extends IBaseService<Admin>{
    Admin findByNumber(@NotNull String number);
}
