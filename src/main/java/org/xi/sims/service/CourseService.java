package org.xi.sims.service;

import org.xi.sims.base.service.IBaseService;
import org.xi.sims.pojo.entity.Course;
import org.xi.sims.pojo.form.CourseForm;
import org.xi.sims.pojo.form.SearchCourseForm;
import org.xi.sims.pojo.vo.CourseVO;
import org.xi.sims.pojo.vo.SelectVO;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author ：xi
 * @time ：Created in 19:09
 * @description：
 */
public interface CourseService extends IBaseService<Course> {
    List<SelectVO> select();

    void deleteCourse(Integer id);

    CourseVO saveOrUpdate(CourseForm form);

    List<CourseVO> query(SearchCourseForm form);

    List<Course> findByName(String name);

    List<CourseVO> findVoByName(String name);

    List<CourseVO> vos(List<Course> courses);

    CourseVO vo(@NotNull Course course);

    CourseVO findVoById(@NotNull Integer id);

    CourseVO findVoByIdAndName(@NotNull Integer id, String name);

    List<CourseVO> findVosByGradeId(@NotNull Integer gradeId);
}
