package org.xi.sims.service;

import org.xi.sims.base.service.IBaseService;
import org.xi.sims.pojo.entity.Grade;
import org.xi.sims.pojo.form.GradeForm;
import org.xi.sims.pojo.form.SearchGradeForm;
import org.xi.sims.pojo.vo.GradeVO;
import org.xi.sims.pojo.vo.SelectVO;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author ：xi
 * @time ：Created in 19:10
 * @description：
 */
public interface GradeService extends IBaseService<Grade> {

    List<SelectVO> select();

    void deleteGrade(Integer id);

    GradeVO saveOrUpdate(GradeForm form);

    GradeVO editNotice(Integer id, String notice);

    List<GradeVO> findVOByTeacherId(Integer teacherId);

    String findGradeNameById(@NotNull Integer id);

    GradeVO vo(@NotNull Grade grade);

    GradeVO findVoById(@NotNull Integer id);

    List<GradeVO> vos(List<Grade> grades);

    List<GradeVO> query(SearchGradeForm form);
}
