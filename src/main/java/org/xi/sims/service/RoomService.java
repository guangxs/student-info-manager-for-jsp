package org.xi.sims.service;

import org.xi.sims.base.service.IBaseService;
import org.xi.sims.pojo.entity.Room;
import org.xi.sims.pojo.form.RoomForm;
import org.xi.sims.pojo.form.SearchRoomForm;
import org.xi.sims.pojo.vo.RoomVO;
import org.xi.sims.pojo.vo.SelectVO;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author ：xi
 * @time ：Created in 19:10
 * @description：
 */
public interface RoomService extends IBaseService<Room> {
    List<SelectVO> select();

    List<RoomVO> query(SearchRoomForm form);

    RoomVO vo(@NotNull Room room);

    List<RoomVO> vos(List<Room> rooms);

    RoomVO saveOrUpdate(RoomForm form);

    void deleteRoom(Integer id);

    RoomVO findVoById(@NotNull Integer id);
}
