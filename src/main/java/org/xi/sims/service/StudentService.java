package org.xi.sims.service;

import org.xi.sims.base.service.IBaseService;
import org.xi.sims.pojo.entity.Student;
import org.xi.sims.pojo.form.SearchStudentForm;
import org.xi.sims.pojo.form.StudentForm;
import org.xi.sims.pojo.vo.StudentVO;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author ：xi
 * @time ：Created in 19:10
 * @description：
 */
public interface StudentService extends IBaseService<Student> {//interface接口

    StudentVO saveOrUpdate(@NotNull StudentForm form);

    Integer findGradeIdById(@NotNull Integer id);

    StudentVO findVoById(@NotNull Integer id);

    StudentVO vo(@NotNull Student student);

    List<StudentVO> findVosByGradeId(@NotNull Integer gradeId);

    List<StudentVO> vos(List<Student> students);

    List<Student> query(SearchStudentForm form);

    Student findByNumber(@NotNull String number);

    StudentVO edit(@NotNull StudentForm form);
}
