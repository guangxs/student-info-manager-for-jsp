package org.xi.sims.service;

import org.xi.sims.base.service.IBaseService;
import org.xi.sims.pojo.entity.Teacher;
import org.xi.sims.pojo.form.SearchTeacherForm;
import org.xi.sims.pojo.form.TeacherForm;
import org.xi.sims.pojo.vo.SelectVO;
import org.xi.sims.pojo.vo.TeacherVO;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author ：xi
 * @time ：Created in 19:10
 * @description：
 */
public interface TeacherService extends IBaseService<Teacher> {
    List<SelectVO> select();

    TeacherVO saveOrUpdate(TeacherForm form);

    TeacherVO edit(@NotNull TeacherForm form);

    Teacher findByNumber(@NotNull String number);

    TeacherVO findVoById(@NotNull Integer id);

    TeacherVO vo(@NotNull Teacher teacher);

    List<TeacherVO> query(SearchTeacherForm search);

    List<TeacherVO> vos(List<Teacher> teachers);

    void deleteTeacher(@NotNull Integer id);
}
