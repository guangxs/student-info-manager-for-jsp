package org.xi.sims.service;

import org.xi.sims.base.service.IBaseService;
import org.xi.sims.pojo.entity.Timetable;
import org.xi.sims.pojo.form.SearchTimetableForm;
import org.xi.sims.pojo.form.TimetableForm;
import org.xi.sims.pojo.vo.TimetableVO;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author ：xi
 * @time ：Created in 19:10
 * @description：
 */
public interface TimetableService extends IBaseService<Timetable> {

    TimetableVO saveOrUpdate(TimetableForm form);

    List<TimetableVO> query(SearchTimetableForm form);

    List<TimetableVO> findVosByRoomId(@NotNull Integer roomId);

    List<TimetableVO> findVosByTeacherId(@NotNull Integer teacherId);

    List<Integer> findCourseIdByGradeId(@NotNull Integer gradeId);

    List<Timetable> findByGradeId(@NotNull Integer gradeId);

    boolean roomIsEmploy(@NotNull Integer roomId);

    List<TimetableVO> findVosByGradeId(@NotNull Integer gradeId);

    List<TimetableVO> findVosByCourseId(@NotNull Integer course);

    List<TimetableVO> findVoByStudentIdAndName(@NotNull Integer studentId, String name);

    List<TimetableVO> findVoByStudentId(@NotNull Integer studentId);

    TimetableVO vo(@NotNull Timetable timetable);

    List<TimetableVO> vos(List<Timetable> timetables);
}
