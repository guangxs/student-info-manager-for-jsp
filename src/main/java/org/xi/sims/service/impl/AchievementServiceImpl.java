package org.xi.sims.service.impl;

import cn.hutool.core.bean.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.xi.sims.base.service.impl.IBaseServiceImpl;
import org.xi.sims.pojo.entity.Achievement;
import org.xi.sims.pojo.vo.AchievementVO;
import org.xi.sims.pojo.vo.CourseVO;
import org.xi.sims.repository.AchievementRepository;
import org.xi.sims.service.*;
import org.xi.sims.util.XinUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：xi
 * @time ：Created in 19:10
 * @description：
 */
@Service
public class AchievementServiceImpl extends IBaseServiceImpl<Achievement, AchievementRepository> implements AchievementService {
    @Autowired
    @Lazy
    private StudentService studentService;
    @Autowired
    @Lazy
    private TeacherService teacherService;
    @Autowired
    @Lazy
    private RoomService roomService;
    @Autowired
    @Lazy
    private CourseService courseService;

    @Override
    public List<AchievementVO> findByStudentId(Integer studentId, String courseName) {
        return new ArrayList<AchievementVO>(){{
            repository.findByStudentId(studentId).forEach(achievement -> {
                CourseVO course = courseService.findVoByIdAndName(achievement.getCourseId(), courseName);
                if (XinUtil.isNull(course)) {
                    return;
                }
                AchievementVO vo = BeanUtil.copyProperties(achievement, AchievementVO.class);
                vo.setCourse(course);
                vo.setStudent(studentService.findVoById(achievement.getStudentId()));
                this.add(vo);
            });
        }};
    }

    @Override
    public List<AchievementVO> findByStudentId(Integer studentId) {
        return vos(repository.findByStudentId(studentId));
    }

    @Override
    public List<AchievementVO> vos(List<Achievement> achievements) {
        return new ArrayList<AchievementVO>() {{
            achievements.forEach(achievement -> {
                this.add(vo(achievement));
            });
        }};
    }

    @Override
    public AchievementVO vo(Achievement achievement) {
        AchievementVO vo = BeanUtil.copyProperties(achievement, AchievementVO.class);
        vo.setCourse(courseService.findVoById(achievement.getCourseId()));
        vo.setStudent(studentService.findVoById(achievement.getStudentId()));
        return vo;
    }

    @Override
    public AchievementVO findByStudentIdAndCourseId(Integer studentId, Integer courseId) {
        Achievement achievement = repository.findByStudentIdAndCourseId(studentId, courseId);
        if (XinUtil.isNull(achievement)) {
            return null;
        }
        return vo(achievement);
    }

    @Override
    public Achievement saveOrUpdate(Achievement achievement) {
        Achievement a = repository.findByStudentIdAndCourseId(achievement.getStudentId(), achievement.getCourseId());
        if (XinUtil.isNull(a)) {
            return repository.save(achievement);
        }
        a.setScore(achievement.getScore());
        return repository.save(a);
    }

}
