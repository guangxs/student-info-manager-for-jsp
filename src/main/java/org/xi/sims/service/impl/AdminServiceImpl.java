package org.xi.sims.service.impl;

import org.springframework.stereotype.Service;
import org.xi.sims.base.service.impl.IBaseServiceImpl;
import org.xi.sims.pojo.entity.Admin;
import org.xi.sims.repository.AdminRepository;
import org.xi.sims.service.AdminService;

/**
 * @author ：xi
 * @time ：Created in 19:10
 * @description：
 */
@Service
public class AdminServiceImpl extends IBaseServiceImpl<Admin, AdminRepository> implements AdminService {
    @Override
    public Admin findByNumber(String number) {
        return repository.findByNumber(number);
    }

}
