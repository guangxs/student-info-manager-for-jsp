package org.xi.sims.service.impl;

import cn.hutool.core.bean.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.xi.sims.base.service.impl.IBaseServiceImpl;
import org.xi.sims.config.exception.CloudException;
import org.xi.sims.pojo.entity.Student;
import org.xi.sims.pojo.form.SearchStudentForm;
import org.xi.sims.pojo.form.StudentForm;
import org.xi.sims.pojo.vo.StudentVO;
import org.xi.sims.repository.StudentRepository;
import org.xi.sims.service.GradeService;
import org.xi.sims.service.StudentService;
import org.xi.sims.util.XinUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author ：xi
 * @time ：Created in 19:10
 * @description：
 */
@Service
public class StudentServiceImpl extends IBaseServiceImpl<Student, StudentRepository> implements StudentService {
    @Autowired
    @Lazy
    private GradeService gradeService;

    @Override
    public StudentVO saveOrUpdate(StudentForm form) {
        Student student = repository.findByNumber(form.getNumber());

        if (XinUtil.isNull(form.getId())) {
            if (XinUtil.noNull(student)) {
                throw new CloudException("学号：" + form.getNumber() + " 已存在!");
            }
            student = BeanUtil.copyProperties(form, Student.class);
            student.setPassword(XinUtil.Md5("123456"));
            return vo(repository.save(student));
        }
        if (XinUtil.noNull(student)) {
            if (!Objects.equals(student.getId(), form.getId())) {
                throw new CloudException("学号：" + form.getNumber() + " 已存在!");
            }
            BeanUtil.copyProperties(form, student);
        } else {
            student = BeanUtil.copyProperties(form, Student.class);
            student.setPassword(XinUtil.Md5("123456"));
        }
        return vo(repository.save(student));
    }

    @Override
    public Integer findGradeIdById(Integer id) {
        Student student = findById(id);
        return student.getGradeId();
    }

    @Override
    public StudentVO findVoById(Integer id) {
        return vo(findById(id));
    }

    @Override
    public StudentVO vo(Student student) {
        StudentVO vo = BeanUtil.copyProperties(student, StudentVO.class);
        vo.setGradeName(gradeService.findGradeNameById(vo.getGradeId()));
        return vo;
    }

    @Override
    public List<StudentVO> findVosByGradeId(Integer gradeId) {
        return vos(repository.findByGradeId(gradeId));
    }

    @Override
    public List<StudentVO> vos(List<Student> students) {
        return new ArrayList<StudentVO>() {{
            students.forEach(student -> {
                this.add(vo(student));
            });
        }};
    }

    @Override
    public List<Student> query(SearchStudentForm form) {
        Student student = Student.builder().build();
        BeanUtil.copyProperties(form, student);
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.contains())//模糊匹配
                .withMatcher("nickname" ,ExampleMatcher.GenericPropertyMatchers.contains())
                .withMatcher("email" ,ExampleMatcher.GenericPropertyMatchers.contains())
                .withMatcher("phone" ,ExampleMatcher.GenericPropertyMatchers.contains())
                .withMatcher("address" ,ExampleMatcher.GenericPropertyMatchers.contains());
        Example<Student> example = Example.of(student, matcher);

        return repository.findAll(example);
    }

    @Override
    public Student findByNumber(String number) {
        return repository.findByNumber(number);
    }

    @Override
    public StudentVO edit(StudentForm form) {
        Student student = findById(form.getId());
        BeanUtil.copyProperties(form, student);
        return vo(repository.save(student));
    }
}
