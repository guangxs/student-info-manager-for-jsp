package org.xi.sims.service.impl;

import cn.hutool.core.bean.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.xi.sims.base.service.impl.IBaseServiceImpl;
import org.xi.sims.config.exception.CloudException;
import org.xi.sims.pojo.entity.Teacher;
import org.xi.sims.pojo.form.SearchTeacherForm;
import org.xi.sims.pojo.form.TeacherForm;
import org.xi.sims.pojo.vo.GradeVO;
import org.xi.sims.pojo.vo.SelectVO;
import org.xi.sims.pojo.vo.TeacherVO;
import org.xi.sims.repository.TeacherRepository;
import org.xi.sims.service.GradeService;
import org.xi.sims.service.TeacherService;
import org.xi.sims.util.XinUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author ：xi
 * @time ：Created in 19:11
 * @description：
 */
@Service
public class TeacherServiceImpl extends IBaseServiceImpl<Teacher, TeacherRepository> implements TeacherService {

    @Autowired
    @Lazy
    private GradeService gradeService;

    @Override
    public List<SelectVO> select() {
        return new ArrayList<SelectVO>() {{
            repository.findAll().forEach( one -> {
                this.add(SelectVO.builder().label(one.getName()).value(one.getId()).build());
            });
        }};
    }

    @Override
    public TeacherVO saveOrUpdate(TeacherForm form) {
        Teacher teacher = repository.findByNumber(form.getNumber());
        if (XinUtil.isNull(form.getId())) {
            if (XinUtil.noNull(teacher)) {
                throw new CloudException("学号：" + form.getNumber() + " 已存在!");
            }
            teacher = BeanUtil.copyProperties(form, Teacher.class);
            teacher.setPassword(XinUtil.Md5("123456"));
            return vo(repository.save(teacher));
        }
        if (XinUtil.noNull(teacher)) {
            if (!Objects.equals(teacher.getId(), form.getId())) {
                throw new CloudException("工号：" + form.getNumber() + " 已存在!");
            }
            BeanUtil.copyProperties(form, teacher);
        } else {
            teacher = BeanUtil.copyProperties(form, Teacher.class);
            teacher.setPassword(XinUtil.Md5("123456"));
        }
        return vo(repository.save(teacher));
    }

    @Override
    public TeacherVO edit(TeacherForm form) {
        Teacher teacher = findById(form.getId());
        BeanUtil.copyProperties(form, teacher);
        return vo(repository.save(teacher));
    }

    @Override
    public Teacher findByNumber(String number) {
        return repository.findByNumber(number);
    }

    @Override
    public TeacherVO findVoById(Integer id) {
        return vo(findById(id));
    }

    @Override
    public TeacherVO vo(Teacher teacher) {
        TeacherVO vo = BeanUtil.copyProperties(teacher, TeacherVO.class);
        return vo;
    }

    @Override
    public List<TeacherVO> query(SearchTeacherForm form) {
        Teacher teacher = Teacher.builder().build();
        BeanUtil.copyProperties(form, teacher);
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.contains())
                .withMatcher("number" ,ExampleMatcher.GenericPropertyMatchers.contains())
                .withMatcher("phone" ,ExampleMatcher.GenericPropertyMatchers.contains())
                .withMatcher("address" ,ExampleMatcher.GenericPropertyMatchers.contains());
        Example<Teacher> example = Example.of(teacher, matcher);
        return vos(repository.findAll(example));
    }


    @Override
    public List<TeacherVO> vos(List<Teacher> teachers) {
        return new ArrayList<TeacherVO>() {{
            teachers.forEach(teacher -> {
                this.add(vo(teacher));
            });
        }};
    }

    @Override
    public void deleteTeacher(Integer id) {
        List<GradeVO> grades = gradeService.findVOByTeacherId(id);
        if (XinUtil.noNull(grades)) {
            throw new CloudException("该教师已在[" +
                    grades.stream().map(GradeVO::getName).collect(Collectors.joining("、"))
                    + "]班级任职，请勿删除!");
        }
        repository.deleteById(id);
    }
}
