package org.xi.sims.util;

import cn.hutool.core.lang.Assert;
import org.xi.sims.common.result.ResultCode;
import org.xi.sims.config.exception.CloudException;

/**
 * @author ：xi
 * @time ：Created in 16:35
 * @description：
 */
public class IAssert extends Assert{

    public static <T> T isNull(T object, ResultCode resultCode) {
        if (XinUtil.isNull(object)) {
            throw new CloudException(resultCode);
        }
        return object;
    }

    public static <T> T noNull(T object, ResultCode resultCode) {
        if (XinUtil.noNull(object)) {
            throw new CloudException(resultCode);
        }
        return object;
    }

    public static void isFalse(boolean object, ResultCode resultCode) {
        if (!object) {
            throw new CloudException(resultCode);
        }
    }
}
