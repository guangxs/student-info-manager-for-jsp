package org.xi.sims.util;


import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.*;

/**
 * @author ：xi
 */
public class XinUtil {
    public static void main(String[] args) {
        System.out.println(Md5("admin".trim(), "longxin"));
//        System.out.println(StrUtil.toUnderlineCase("parentId"));
//        System.out.println(IdUtil.simpleUUID());
//        System.out.println(IdUtil.simpleUUID());
//        System.out.println(DateUtil.parseDate("1979-02-03"));
//        System.out.println(DateUtil.parseDate("2000/03/01"));
//
//        List<String> s = new ArrayList<>(){{
//            this.add("");
//        }};
//        System.out.println(ObjectUtil.isNull(s));
    }

    public static <T> T selectNoNull(T...vars) {
        for (T var : vars) {
            if (noNull(var)) {
                return var;
            }
        } return null;
    }

    /*public static Object selectNoNull(Object...vars) {
        for (Object var : vars) {
            if (noNull(var)) {
                return var;
            }
        } return null;
    }*/


    public static boolean isNull(Object var){
        return ObjectUtil.isEmpty(var);
    }

    public static boolean noNull(Object var){
        return !isNull(var);
    }


    public static boolean noAllNull(Object...vars){
        for (Object var : vars) {
            if (isNull(var)) {
                return false;
            }
        } return true;
    }

    public static boolean hasNull(Object...vars){
        return !noAllNull(vars);
    }

    public static boolean isAllNull(Object...vars){
        for (Object var : vars) {
            if (noNull(var)) {
                return false;
            }
        } return true;
    }

    public static String trimAll(String var){  return isNull(var) ? "" : var.replaceAll(" ", ""); }

    public static  <T> Set<T> set(List<T> list){
        return new HashSet<>(list);
    }

    public static <T> List<T> list(Set<T> sets) {
        return new ArrayList<>(sets);
    }

    public static boolean isZero(Integer var){
        return !isNull(var) && var == 0;
    }

    public static boolean noZero(Integer var){
        return !isZero(var);
    }

    public static String toStr(String...var){
        StringBuilder sb = new StringBuilder();
        for (String ele : var) {
            sb.append(ele);
        }
        return sb.toString();
    }


    public static boolean hasEqual(final CharSequence var, final CharSequence...vars){
        if (isNull(var)) return false;
        for (CharSequence v : vars){
            if (equal(var, v)) return true;
        }
        return false;
    }

    public static boolean equal(final CharSequence cs1, final CharSequence cs2){
        return StrUtil.equals(cs1, cs2);
    }

    public static boolean noEqual(final CharSequence cs1, final CharSequence cs2){
        return !equal(cs1, cs2);
    }

    public static boolean equal(final Object var1, final Object var2){
        return ObjectUtil.equal(var1, var2);
    }

    public static boolean noEqual(final Object var1, final Object var2){
        return !equal(var1, var2);
    }

    public static boolean isEmail(String var) {
        if (isNull(var)) {
            return false;
        }
        String regEx1 = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
        /*Pattern p;
        Matcher m;
        p = Pattern.compile(regEx1);
        m = p.matcher(var);
        if (m.matches()) {
            return true;
        }*/
        return ReUtil.isMatch("\\w+[\u4E00-\u9FFF]+\\d+", regEx1);
    }

    public static boolean noEmail(String var) {
        return !isEmail(var);
    }


    /**
     * 是否包含字符串
     *
     * @param str  验证字符串
     * @param strs 字符串组
     * @return 包含返回true
     */
    public static boolean inStringIgnoreCase(String str, String... strs) {
        if (str != null && strs != null) {
            for (String s : strs) {
                if (str.equalsIgnoreCase(StrUtil.trim(s))) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @description: 获取字段上的某注解
     * @remarks:
     * @author ：xi
     * @date 2021/5/17 8:30
     */
    public static  <T extends Annotation> T getFieldAnnotation(Field field, Class<T> clazz){
        return field.getAnnotation(clazz);
    }


    /**
     * @description: 字段类型
     * @remarks:
     * @author ：xi
     * @date 2021/5/17 8:30
     */
    public static String getFieldType(Field field){
        String classType = field.getType().toString();
        int lastIndex = classType.lastIndexOf(".");
        return classType.substring(lastIndex + 1);
    }

    /**
     * @description: 获取类的字段集合
     * @remarks:
     * @author ：xi
     * @date 2021/5/17 8:30
     */
    public static List<Field> getFieldList(Class<?> clazz){
        //获取当前类所有字段
        List<Field> fields = new ArrayList<>(Arrays.asList(clazz.getDeclaredFields()));
        //获取父类class
        Class<?> superClazz = clazz.getSuperclass();
        if (isNull(superClazz)){
            return fields;
        }
        List<Field> superFields = new ArrayList<>(Arrays.asList(superClazz.getDeclaredFields()));
        if (superFields.size() >= 1) {
            fields.addAll(superFields);
        }
        return fields;
    }

    /**
     * @description: 获取类的字段集合
     * @remarks:
     * @author ：xi
     * @date 2021/5/17 8:30
     */
    public static <T extends Annotation> List<Field> getFieldListInAnnotation(Class<?> clazz, Class<T> annotation){
        List<Field> resultFields = getFieldList(clazz);
        List<Field> fields = new ArrayList<>();
        resultFields.forEach(field -> {
            if (noNull(getFieldAnnotation(field, annotation))) {
                fields.add(field);
            }
        });
        return fields;
    }



    private final static byte[] KEY = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();

    /**
     * @description: AES加密
     * @remarks:
     * @author ：xi
     * @date 2021/5/17 8:28
     */
    public static String AesE(String decrypt){
        return AesE(decrypt, KEY);
    }

    public static String AesE(String decrypt, byte[] key){
        //构建
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, key);
        //加密为16进制表示
        return aes.encryptHex(decrypt) ;
    }

    /**
     * @description: AES解密
     * @remarks:
     * @author ：xi
     * @date 2021/5/17 8:29
     */
    public static String AesD(String encrypt){
        return AesD(encrypt, KEY);
    }

    public static String AesD(String encrypt, byte[] key){
        //构建
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, key);
        return aes.decryptStr(encrypt, CharsetUtil.CHARSET_UTF_8);
    }

    public static String Md5(String decrypt, String salt) {
        return SecureUtil.md5(decrypt + salt);
    }

    private final static String SALT = "xxxxxxx";

    public static String Md5(String encrypt) {
        return Md5(encrypt, SALT);
    }
}

