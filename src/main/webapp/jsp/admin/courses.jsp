<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<jsp:include page="../common/Aheader.jsp" flush="true">
  <jsp:param name="title" value="${title}"/>
  <jsp:param name="active" value="3"/>
</jsp:include>

<div id="app">
  <el-card class="box-card" shadow="hover">
    <el-form :inline="true" ref="searchForm" :model="searchForm" label-width="80px">
      <el-form-item label="课程号">
        <el-input v-model="searchForm.number"></el-input>
      </el-form-item>
      <el-form-item label="课程名">
        <el-input v-model="searchForm.name"></el-input>
      </el-form-item>
      <el-form-item label="课时">
        <el-input v-model="searchForm.learnTime"></el-input>
      </el-form-item>
      <el-form-item label="学分">
        <el-input v-model="searchForm.credit"></el-input>
      </el-form-item>
    </el-form>
  </el-card>
  <el-card class="box-card" shadow="hover">
    <span style="font-size: 20px">
             <el-button type="primary" style="margin-left: 10px" icon="el-icon-search" @click="search"></el-button>
             <el-button type="primary" style="margin-left: 10px" icon="el-icon-refresh" @click="list"></el-button>
             <el-button type="primary" icon="el-icon-plus" @click="handleInsertCourse">添加课程</el-button>
    </span>
    <el-table :data="courses" style="width: 100%">
      <el-table-column prop="number" label="课程号"></el-table-column>
      <el-table-column prop="name" label="课程名"></el-table-column>
      <el-table-column prop="learnTime" label="课时"></el-table-column>
      <el-table-column prop="credit" label="学分"></el-table-column>
      <el-table-column label="操作">
        <template slot-scope="scope">
          <el-button size="mini" @click="handleEditCourse(scope.row)">编辑</el-button>
          <el-button size="mini" type="danger" @click="handleDeleteCourse(scope.row)">删除</el-button>
        </template>
      </el-table-column>
    </el-table>
  </el-card>
  <el-drawer :title="courseForm.id === undefined ? '添加课程信息' : '修改课程信息'" :visible.sync="drawerEditCourse"
             direction="rtl" size="50%">
    <el-card class="box-card" shadow="hover">
      <el-form ref="courseForm" :model="courseForm" label-width="80px">
        <el-form-item label="课程名">
          <el-input v-model="courseForm.name"></el-input>
        </el-form-item>
        <el-form-item label="课程号" >
          <el-input v-model="courseForm.number"></el-input>
        </el-form-item>
        <el-form-item label="学时">
          <el-input v-model="courseForm.learnTime"></el-input>
        </el-form-item>
        <el-form-item label="学分">
          <el-input v-model="courseForm.credit"></el-input>
        </el-form-item>
        <el-form-item>
          <el-button type="primary" @click="saveOrUpdateCourse">提 交</el-button>
          <el-button>取 消</el-button>
        </el-form-item>
      </el-form>
    </el-card>
  </el-drawer>
</div>

<script>
  new Vue({
    el: '#app',
    data() {
      return {
        courses: [],
        courseForm: {},
        drawerEditCourse: false,
        searchForm: {}
      }
    },
    created() {
      this.list();
    },
    methods: {
      search(){
        axios.post('/api/course/courses', this.searchForm).then(res => {
          const {code, data, message} = res.data
          if (code === 200) {
            this.courses = data
          } else {
            this.$message({
              message: message,
              type: 'error'
            });
          }
        })
      },
      handleEditCourse(row) {
        this.courseForm = row
        this.drawerEditCourse = true
      },
      handleDeleteCourse(row) {
        axios.delete('/api/course/deleteCourse/' + row.id).then(res => {
          const {code,  message} = res.data
          if (code === 200) {
            this.list()
            this.$message({
              message: message,
              type: 'success'
            });
          } else {
            this.$message({
              message: message,
              type: 'error'
            });
          }
        })
      },
      handleInsertCourse() {
        this.courseForm = {}
        this.drawerEditCourse = true
      },
      saveOrUpdateCourse() {
        axios.post('/api/course/saveOrUpdate', this.courseForm).then(res => {
          const {code, data, message} = res.data
          if (code === 200) {
            this.list()
            this.$message({
              message: message,
              type: 'success'
            });
          } else {
            this.$message({
              message: message,
              type: 'error'
            });
          }
        })
      },
      list(){
        this.searchForm = {}
        axios.post('/api/course/courses', {}).then(res => {
          const {code, data, message} = res.data
          if (code === 200) {
            this.courses = data
          } else {
            this.$message({
              message: message,
              type: 'error'
            });
          }
        })
      },
    },
  });
</script>

<%@include file="../common/footer.jsp" %>