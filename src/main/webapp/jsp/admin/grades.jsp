<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<jsp:include page="../common/Aheader.jsp" flush="true">
    <jsp:param name="title" value="${title}"/>
    <jsp:param name="active" value="1"/>
</jsp:include>

<div id="app">
    <el-card class="box-card" shadow="hover">
        <el-form :inline="true" ref="searchForm" :model="searchForm" label-width="80px">
            <el-form-item label="班级号">
                <el-input v-model="searchForm.number"></el-input>
            </el-form-item>
            <el-form-item label="班级名">
                <el-input v-model="searchForm.name"></el-input>
            </el-form-item>
        </el-form>
    </el-card>
    <el-card class="box-card" shadow="hover">
        <span style="font-size: 20px">
            <el-button type="primary" style="margin-left: 10px" icon="el-icon-search" @click="search"></el-button>
             <el-button type="primary" style="margin-left: 10px" icon="el-icon-refresh" @click="list"></el-button>
             <el-button type="primary" icon="el-icon-plus" @click="handleInsertGrade">添加班级</el-button>
        </span>
        <el-table :data="grades" style="width: 100%">
            <el-table-column prop="number" label="班级号"></el-table-column>
            <el-table-column prop="name" label="姓名"></el-table-column>
            <el-table-column prop="notice" label="公告"></el-table-column>
            <el-table-column prop="teacherId" label="班主任">
                <template slot-scope="scope">
                    {{scope.row.teacher.name}}
                </template>
            </el-table-column>

            <el-table-column label="操作">
                <template slot-scope="scope">
                    <el-button size="mini" @click="handleEditGrade(scope.row)">编辑</el-button>
                    <el-button size="mini" type="danger" @click="handleDeleteGrade(scope.row)">删除</el-button>
                </template>
            </el-table-column>
        </el-table>
    </el-card>
    <el-drawer :title="graderForm.id === undefined ? '添加班级信息' : '修改班级信息'" :visible.sync="drawerEditGrade"
               direction="rtl" size="50%">
        <el-card class="box-card" shadow="hover">
            <el-form ref="graderForm" :model="graderForm" label-width="80px">
                <el-form-item label="班级名">
                    <el-input v-model="graderForm.name"></el-input>
                </el-form-item>
                <el-form-item label="班级号">
                    <el-input v-model="graderForm.number"></el-input>
                </el-form-item>
                <el-form-item label="班级公告">
                    <el-input v-model="graderForm.notice"></el-input>
                </el-form-item>
                <el-form-item label="班主任">
                    <el-select v-model="graderForm.teacherId" placeholder="请选择">
                        <el-option v-for="teacher in teachers" :key="teacher.id" :label="teacher.name" :value="teacher.id"></el-option>
                    </el-select>
                </el-form-item>

                <el-form-item>
                    <el-button type="primary" @click="saveOrUpdateGrade">提 交</el-button>
                    <el-button>取 消</el-button>
                </el-form-item>
            </el-form>
        </el-card>
    </el-drawer>
</div>

<script>
    new Vue({
        el: '#app',
        data() {
            return {
                grades: [],
                teachers: [],
                graderForm: {},
                drawerEditGrade: false,
                searchForm: {}

        }
        },
        created() {
            this.list();
            this.getTeachers();

        },
        methods: {
            search(){
                axios.post('/api/grade/grades', this.searchForm).then(res => {
                    const {code, data, message} = res.data
                    if (code === 200) {
                        this.grades = data
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            },
            handleEditGrade(row) {
                this.graderForm = row
                this.drawerEditGrade = true
            },
            handleDeleteGrade(row) {
                axios.delete('/api/grade/deleteGrade/' + row.id).then(res => {
                    const {code,  message} = res.data
                    if (code === 200) {
                        this.list()
                        this.$message({
                            message: message,
                            type: 'success'
                        });
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            },
            handleInsertGrade() {
                this.graderForm = {}
                this.drawerEditGrade = true
            },
            saveOrUpdateGrade() {
                axios.post('/api/grade/saveOrUpdate', this.graderForm).then(res => {
                    const {code, data, message} = res.data
                    if (code === 200) {
                        this.list()
                        this.$message({
                            message: message,
                            type: 'success'
                        });
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            },
            list(){
                this.searchForm = {}
                axios.post('/api/grade/grades', {}).then(res => {
                    const {code, data, message} = res.data
                    if (code === 200) {
                        this.grades = data
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            },
            getTeachers() {
                axios.post('/api/teacher/teachers', {}).then(res => {
                    const {code, data, message} = res.data
                    if (code === 200) {
                        this.teachers = data
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            }
        },
    });
</script>

<%@include file="../common/footer.jsp" %>