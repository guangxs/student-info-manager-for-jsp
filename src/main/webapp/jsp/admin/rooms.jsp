<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<jsp:include page="../common/Aheader.jsp" flush="true">
    <jsp:param name="title" value="${title}"/>
    <jsp:param name="active" value="2"/>
</jsp:include>

<div id="app">
    <el-card class="box-card" shadow="hover">
        <el-form :inline="true" ref="searchForm" :model="searchForm" label-width="80px">
            <el-form-item label="教室号">
                <el-input v-model="searchForm.number"></el-input>
            </el-form-item>
            <el-form-item label="教学楼">
                <el-input v-model="searchForm.level"></el-input>
            </el-form-item>
            <el-form-item label="教室">
                <el-input v-model="searchForm.room"></el-input>
            </el-form-item>
        </el-form>
    </el-card>
    <el-card class="box-card" shadow="hover">
        <span style="font-size: 20px">
            <el-button type="primary" style="margin-left: 10px" icon="el-icon-search" @click="search"></el-button>
             <el-button type="primary" style="margin-left: 10px" icon="el-icon-refresh" @click="list"></el-button>
             <el-button type="primary" icon="el-icon-plus" @click="handleInsertRoom">添加教室</el-button>
        </span>
        <el-table :data="rooms" style="width: 100%">
            <el-table-column prop="number" label="教室号"></el-table-column>
            <el-table-column prop="level" label="教学楼"></el-table-column>
            <el-table-column prop="room" label="教室"></el-table-column>
            <el-table-column label="操作">
                <template slot-scope="scope">
                    <el-button size="mini" @click="handleEditRoom(scope.row)">编辑</el-button>
                    <el-button size="mini" type="danger" @click="handleDeleteRoom(scope.row)">删除</el-button>
                </template>
            </el-table-column>
        </el-table>
    </el-card>
    <el-drawer :title="roomForm.id === undefined ? '添加教室信息' : '修改教室信息'" :visible.sync="drawerEditRoom"
               direction="rtl" size="50%">
        <el-card class="box-card" shadow="hover">
            <el-form ref="roomForm" :model="roomForm" label-width="80px">
                <el-form-item label="教学楼">
                    <span>{{roomForm.level}}</span>
                    <el-cascader v-model="roomForm.level" :options="roomList" :props="{ expandTrigger: 'hover' }" @change="handleChange"></el-cascader>
                </el-form-item>
                <el-form-item label="教室序号" >
                    <el-input v-model="roomForm.room"  :disabled="true"></el-input>
                </el-form-item>
                <el-form-item label="教室号">
                    <el-input v-model="roomForm.number"  :disabled="true"></el-input>
                </el-form-item>
                <el-form-item>
                    <el-button type="primary" @click="saveOrUpdateRoom">提 交</el-button>
                    <el-button>取 消</el-button>
                </el-form-item>
            </el-form>
        </el-card>
    </el-drawer>
</div>

<script>
    new Vue({
        el: '#app',
        data() {
            return {
                rooms: [],
                roomForm: {},
                drawerEditRoom: false,
                roomList: [],
                code: '',
                searchForm: {}
            }
        },
        created() {
            this.list();
            axios.get("/json/Room.json").then((res) => {
                this.roomList = res.data
            })
        },
        methods: {
            search(){
                axios.post('/api/room/rooms', this.searchForm).then(res => {
                    const {code, data, message} = res.data
                    if (code === 200) {
                        this.rooms = data
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            },
            handleChange(value){
                console.log(value)
                const rooms = this.roomList
                for (let i in rooms) {
                    if (rooms[i].value === value[0]) {
                        this.roomForm.level = rooms[i].label
                        this.roomForm.room = value[2]
                        this.roomForm.number = rooms[i].value + value[2]
                    }
                }

            },
            handleEditRoom(row) {
                this.roomForm = row
                this.drawerEditRoom = true
            },
            handleDeleteRoom(row) {
                axios.delete('/api/room/deleteRoom/' + row.id).then(res => {
                    const {code,  message} = res.data
                    if (code === 200) {
                        this.list()
                        this.$message({
                            message: message,
                            type: 'success'
                        });
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            },
            handleInsertRoom() {
                this.roomForm = {}
                this.drawerEditRoom = true
            },
            saveOrUpdateRoom() {
                axios.post('/api/room/saveOrUpdate', this.roomForm).then(res => {
                    const {code, data, message} = res.data
                    if (code === 200) {
                        this.list()
                        this.$message({
                            message: message,
                            type: 'success'
                        });
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            },
            list(){
                this.searchForm = {}
                axios.post('/api/room/rooms', {}).then(res => {
                    const {code, data, message} = res.data
                    if (code === 200) {
                        this.rooms = data
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            },
        },
    });
</script>

<%@include file="../common/footer.jsp" %>