<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<jsp:include page="../common/Aheader.jsp" flush="true">
    <jsp:param name="title" value="${title}"/>
    <jsp:param name="active" value="0"/>
</jsp:include>

<div id="app">
    <el-card class="box-card" shadow="hover">
        <el-form :inline="true" ref="searchForm" :model="searchForm" label-width="80px">
            <el-form-item label="工号">
                <el-input v-model="searchForm.number"></el-input>
            </el-form-item>
            <el-form-item label="姓名">
                <el-input v-model="searchForm.name"></el-input>
            </el-form-item>
            <el-form-item label="职称">
                <el-input v-model="searchForm.leader"></el-input>
            </el-form-item>
            <el-form-item label="性别">
                <el-radio-group v-model="searchForm.sex">
                    <el-radio label="男"></el-radio>
                    <el-radio label="女"></el-radio>
                </el-radio-group>
            </el-form-item>
            <el-form-item label="电话">
                <el-input v-model="searchForm.phone"></el-input>
            </el-form-item>
            <el-form-item label="地址">
                <el-input v-model="searchForm.address"></el-input>
            </el-form-item>
        </el-form>
    </el-card>
    <el-card class="box-card" shadow="hover">
        <span style="font-size: 20px">
            <el-button type="primary" style="margin-left: 10px" icon="el-icon-search" @click="search"></el-button>
             <el-button type="primary" style="margin-left: 10px" icon="el-icon-refresh" @click="list"></el-button>
             <el-button type="primary" icon="el-icon-plus" @click="handleInsertTeacher">添加教师</el-button>
        </span>
        <el-table :data="teachers" style="width: 100%">
            <el-table-column prop="number" label="工号"></el-table-column>
            <el-table-column prop="name" label="姓名"></el-table-column>
            <el-table-column prop="leader" label="职称"></el-table-column>
            <el-table-column prop="avatar" label="头像">
                <template slot-scope="scope">
                    <el-avatar :src="scope.row.avatar"></el-avatar>
                </template>
            </el-table-column>
            <el-table-column prop="sex" label="性别"></el-table-column>
            <el-table-column prop="phone" label="电话"></el-table-column>
            <el-table-column prop="address" label="地址"></el-table-column>
            <el-table-column prop="birth" label="生日"></el-table-column>
            <el-table-column label="操作">
                <template slot-scope="scope">
                    <el-button size="mini" @click="handleEditTeacher(scope.row)">编辑</el-button>
                    <el-button size="mini" type="danger" @click="handleDeleteTeacher(scope.row)">删除</el-button>
                </template>
            </el-table-column>
        </el-table>
    </el-card>
    <el-drawer :title="teacherForm.id === undefined ? '添加教师信息' : '修改教师信息'" :visible.sync="drawerEditTeacher"
               direction="rtl" size="50%">
        <el-card class="box-card" shadow="hover">
            <el-form ref="teacherForm" :model="teacherForm" label-width="80px">
                <el-form-item label="教师姓名">
                    <el-input v-model="teacherForm.name"></el-input>
                </el-form-item>
                <el-form-item label="工号">
                    <el-input v-model="teacherForm.number"></el-input>
                </el-form-item>
                <el-form-item label="职称">
                    <el-radio-group v-model="teacherForm.leader">
                        <el-radio label="讲师"></el-radio>
                        <el-radio label="教授"></el-radio>
                    </el-radio-group>
                </el-form-item>
                <el-form-item label="性别">
                    <el-radio-group v-model="teacherForm.sex">
                        <el-radio label="男"></el-radio>
                        <el-radio label="女"></el-radio>
                    </el-radio-group>
                </el-form-item>
                <el-form-item>
                    <el-button type="primary" @click="saveOrUpdateTeacher">提 交</el-button>
                    <el-button>取 消</el-button>
                </el-form-item>
            </el-form>
        </el-card>
    </el-drawer>
</div>


<script>

    new Vue({
        el: '#app',
        data() {
            return {
                teachers: [],
                teacherForm: {},
                drawerEditTeacher: false,
                searchForm: {}
            }
        },
        created() {
            this.list()
        },
        methods: {
            search(){
                axios.post('/api/teacher/teachers', this.searchForm).then(res => {
                    const {code, data, message} = res.data
                    if (code === 200) {
                        this.teachers = data
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            },
            list(){
                this.searchForm = {}
                axios.post('/api/teacher/teachers', {}).then(res => {
                    const {code, data, message} = res.data
                    if (code === 200) {
                        this.teachers = data
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            },
            handleInsertTeacher() {
                this.teacherForm = {}
                this.drawerEditTeacher = true
            },
            handleEditTeacher(row) {
                this.teacherForm = row
                this.drawerEditTeacher = true
            },
            handleDeleteTeacher(row) {
                axios.delete('/api/teacher/deleteTeacher/' + row.id).then(res => {
                    const {code,  message} = res.data
                    if (code === 200) {
                        this.list()
                        this.$message({
                            message: message,
                            type: 'success'
                        });
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            },
            saveOrUpdateTeacher() {
                axios.post('/api/teacher/saveOrUpdate', this.teacherForm).then(res => {
                    const {code, data, message} = res.data
                    if (code === 200) {
                        this.list()
                        this.$message({
                            message: message,
                            type: 'success'
                        });
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            }
        },
    });
</script>

<%@include file="../common/footer.jsp" %>