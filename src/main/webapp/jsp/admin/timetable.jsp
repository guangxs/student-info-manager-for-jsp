<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<jsp:include page="../common/Aheader.jsp" flush="true">
    <jsp:param name="title" value="${title}"/>
    <jsp:param name="active" value="4"/>
</jsp:include>

<div id="app">
    <el-card class="box-card" shadow="hover">
        <span style="font-size: 20px">
            <el-button type="primary" style="margin-left: 10px" icon="el-icon-search" @click="search"></el-button>
             <el-button type="primary" style="margin-left: 10px" icon="el-icon-refresh" @click="list"></el-button>
             <el-button type="primary" icon="el-icon-plus" @click="handleInsert">添加课程安排</el-button>
        </span>
        <el-table :data="dataList" style="width: 100%">
            <el-table-column prop="grade.name" label="班级"></el-table-column>
            <el-table-column prop="teacher.name" label="教师"></el-table-column>
            <el-table-column prop="room.number" label="教室"></el-table-column>
            <el-table-column prop="start" label="授课起始周"></el-table-column>
            <el-table-column prop="end" label="授课结束周"></el-table-column>
            <el-table-column prop="week" label="上课周"></el-table-column>
            <el-table-column prop="knob" label="上课节"></el-table-column>
            <el-table-column label="操作">
                <template slot-scope="scope">
                    <el-button size="mini" @click="handleEdit(scope.row)">编辑</el-button>
                    <el-button size="mini" type="danger" @click="handleDelete(scope.row)">删除</el-button>
                </template>
            </el-table-column>
        </el-table>
    </el-card>
    <el-drawer :title="form.id === undefined ? '添加课程安排信息' : '修改课程安排信息'" :visible.sync="drawerForm"
               direction="rtl" size="50%">
        <el-card class="box-card" shadow="hover">
            <el-form ref="form" :model="form" label-width="80px">
                <el-form-item label="上课班级">
                    <el-select v-model="form.gradeId" placeholder="请选择">
                        <el-option v-for="item in grades" :key="item.value" :label="item.label" :value="item.value"></el-option>
                    </el-select>
                </el-form-item>
                <el-form-item label="课程" >
                    <el-select v-model="form.courseId" placeholder="请选择">
                        <el-option v-for="item in courses" :key="item.value" :label="item.label" :value="item.value"></el-option>
                    </el-select>
                </el-form-item>
                <el-form-item label="上课教室" >
                    <el-select v-model="form.roomId" placeholder="请选择">
                        <el-option v-for="item in rooms" :key="item.value" :label="item.label" :value="item.value"></el-option>
                    </el-select>
                </el-form-item>
                <el-form-item label="授课教师" >
                    <el-select v-model="form.teacherId" placeholder="请选择">
                        <el-option v-for="item in teachers" :key="item.value" :label="item.label" :value="item.value"></el-option>
                    </el-select>
                </el-form-item>
                <el-form-item label="授课起始周" >
                    <el-input v-model="form.start" ></el-input>
                </el-form-item>
                <el-form-item label="授课结束周" >
                    <el-input v-model="form.end" ></el-input>
                </el-form-item>
                <el-form-item label="上课周" >
                    <el-select v-model="form.week" placeholder="请选择">
                        <el-option v-for="item in weeks" :key="item.value" :label="item.label" :value="item.value"></el-option>
                    </el-select>
                </el-form-item>
                <el-form-item label="上课节" >
                    <el-select v-model="form.knob" placeholder="请选择">
                        <el-option v-for="item in knobs" :key="item.value" :label="item.label" :value="item.value"></el-option>
                    </el-select>
                </el-form-item>
                <el-form-item>
                    <el-button type="primary" @click="saveOrUpdate">提 交</el-button>
                    <el-button>取 消</el-button>
                </el-form-item>
            </el-form>
        </el-card>
    </el-drawer>
</div>

<script>

    new Vue({
        el: '#app',
        data() {
            return {
                form: {},
                drawerForm: false,
                dataList: [],
                teachers: [],
                rooms: [],
                courses: [],
                grades: [],
                weeks: [{label:'周一',value: 1},{label:'周二',value: 2},{label:'周三',value: 3},{label:'周四',value: 4},
                    {label:'周五',value: 5},{label:'周六',value: 6},{label:'周日',value: 7}],
                knobs: [{label:'第一节',value: 1},{label:'第二节',value: 2},{label:'第三节',value: 3},{label:'第四节',value: 4},{label:'第五节',value: 5},],
                searchForm: {}
            }
        },
        created() {
            this.list();
            axios.get('/api/course/select').then(res=>{
                const {code, data, message} = res.data
                if (code === 200) {
                    this.courses = data
                } else {
                    this.$message({
                        message: message,
                        type: 'error'
                    });
                }
            });
            axios.get('/api/teacher/select').then(res=>{
                const {code, data, message} = res.data
                if (code === 200) {
                    this.teachers = data
                } else {
                    this.$message({
                        message: message,
                        type: 'error'
                    });
                }
            });
            axios.get('/api/room/select').then(res=>{
                const {code, data, message} = res.data
                if (code === 200) {
                    this.rooms = data
                } else {
                    this.$message({
                        message: message,
                        type: 'error'
                    });
                }
            });
            axios.get('/api/grade/select').then(res=>{
                const {code, data, message} = res.data
                if (code === 200) {
                    this.grades = data
                } else {
                    this.$message({
                        message: message,
                        type: 'error'
                    });
                }
            });
        },
        methods: {
            search(){
                axios.post('/api/timetable/timetables', this.searchForm).then(res => {
                    const {code, data, message} = res.data
                    if (code === 200) {
                        this.dataList = data
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            },
            handleChange(value){
                
            },
            handleEdit(row) {
                this.form = row
                this.drawerForm = true
            },
            handleDelete(row) {
                axios.delete('/api/timetable/delete/' + row.id).then(res => {
                    const {code,  message} = res.data
                    if (code === 200) {
                        this.list()
                        this.$message({
                            message: message,
                            type: 'success'
                        });
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            },
            handleInsert() {
                this.form = {}
                this.drawerForm = true
            },
            saveOrUpdate() {
                axios.post('/api/timetable/saveOrUpdate', this.form).then(res => {
                    const {code, data, message} = res.data
                    if (code === 200) {
                        this.$message({
                            message: message,
                            type: 'success'
                        });
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                    this.list()
                })
            },
            list(){
                this.searchForm = {}
                axios.post('/api/timetable/timetables', {}).then(res => {
                    const {code, data, message} = res.data
                    if (code === 200) {
                        this.dataList = data
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            },
        },
    });
</script>

<%@include file="../common/footer.jsp" %>