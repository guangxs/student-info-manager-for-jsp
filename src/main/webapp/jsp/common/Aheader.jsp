<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/css/element.css">
    <%--    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">--%>
    <script type="text/javascript" src="/js/vue.js"></script>
    <script type="text/javascript" src="/js/axios.min.js"></script>
    <script type="text/javascript" src="/js/element.js"></script>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <title>${param.title}</title>
</head>
<body>
<div>
    <jsp:include page="../components/Amenu.jsp" flush="true">
        <jsp:param name="active" value="${param.active}"/>
        <jsp:param name="title" value="${param.title}" />
    </jsp:include>
</div>

