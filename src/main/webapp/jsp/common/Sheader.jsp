<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>  
 
<meta charset="UTF-8">    
<link rel="stylesheet" href="/css/vant.css" />
<script type="text/javascript" src="/js/vue.js"></script>
<script type="text/javascript" src="/js/vant.min.js"></script>
<script type="text/javascript" src="/js/axios.min.js"></script>
<title>${param.title}</title>
<style scoped>
.layout {
	/*flex-direction: row-reverse;*/
	background-image: url('/images/background.jpg');
	height: 100vh;
}
</style>
</head>
<body>

<div>
    <jsp:include page="../components/Stop.jsp" flush="true">
        <jsp:param name="title" value="${param.title}" />
    </jsp:include>
    <jsp:include page="../components/Smenu.jsp" flush="true">
        <jsp:param name="active" value="${param.active}"/>
    </jsp:include>
</div>


