<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="menu">
    <el-tabs v-model="active" type="card" @tab-click="clickTab">
        <el-tab-pane v-for="item in options" :name="item.value">
            <span slot="label"><i :class="item.icon"></i> {{item.label}}</span>
        </el-tab-pane>
    </el-tabs>
</div>

<jsp:include page="../components/Atop.jsp" flush="true">
    <jsp:param name="title" value="${param.title}" />
</jsp:include>
<br>

<script>
    new Vue({
        el: '#menu',

        data() {
            return {
                active: ${param.active}+'' +
                    '',
                options: [
                    {
                        label: "教师管理",
                        value: '0',
                        icon: 'el-icon-user',
                        url: '/admin/teachers'
                    }, {
                        label: "班级管理",
                        value: '1',
                        icon: 'el-icon-wind-power',
                        url: '/admin/grades'
                    }, {
                        label: "教室管理",
                        value: '2',
                        icon: 'el-icon-school',
                        url: '/admin/rooms'
                    },
                    {
                        label: "课程管理",
                        value: '3',
                        icon: 'el-icon-reading',
                        url: '/admin/courses'
                    },
                    {
                        label: "课程安排",
                        value: '4',
                        icon: 'el-icon-watch',
                        url: '/admin/timetable'
                    },
                    {
                        label: "设 置",
                        value: '5',
                        icon: 'el-icon-setting',
                        url: '/admin/setting'
                    },
                ],
            }
        },
        created() {

        },
        methods: {
            clickTab(tab, event) {
                window.location = (this.options.find(x=>x.value === tab.name)?.url)
            },
        },
    });
</script>