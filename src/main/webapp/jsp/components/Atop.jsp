<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="top">
    <el-page-header @back="goBack" content="${param.title}"></el-page-header>
</div>

<script>
    new Vue({
        el: '#top',

        data() {
            return {

            }
        },
        created() {

        },
        methods: {
            goBack() {
                window.history.go(-1)
            },
        },
    });
</script>