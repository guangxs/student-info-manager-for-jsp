<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


<div id="menu">
	<van-tabbar v-if="checkActive(active)" v-model="active">
		<van-tabbar-item v-for="item in menus" :icon="item.icon" replace :url="item.url">
			{{ item.title }}
		</van-tabbar-item>
	</van-tabbar>
</div>

<script>
	new Vue({
		el: '#menu',

		data() {
			return {
				active: ${param.active} + '',
				menus: [
					{ icon: 'orders-o', url:'/student/course', title: '课程' },
					{ icon: 'search', url:'/student/score', title: '成绩' },
					{ icon: 'flag-o', url:'/student/grade', title: '班级' },
					{ icon: 'user-o', url:'/student/info', title: '我的' },
				],
			}
		},
		created() {
		},
		methods: {
			checkActive(active) {
				if (active !== undefined && active !== null) {
					if (parseInt(active) >= 0 && parseInt(active) <= 4) {
						this.active = parseInt(active)
						return true
					}
				} return false
			}
		},
	});
</script>