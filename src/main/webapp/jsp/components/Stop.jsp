<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="top">
    <van-sticky>
        <van-nav-bar right-text="设置" left-arrow title='${param.title}' @click-left="onClickLeft" @click-right="showShare = true"></van-nav-bar>
    </van-sticky>

    <van-share-sheet v-model="showShare" :options="options" @select="onSelect"></van-share-sheet>
    <div v-if="editPwdShow">
        <van-popup v-model="editPwdShow" position="bottom" round duration="1" :style="{ height: '80%' }" closeable close-icon="arrow-down">
            <van-row>
                <van-col offset="0" span="24">
                    <div class="from">
                        <van-form @submit="onSubmit">
                            <van-field v-model="editPwdForm.originalPassword" type="password" label="原密码" placeholder="原密码" :rules="[{ required: true, message: '原密码不能为空' }]"></van-field>
                            <van-field v-model="editPwdForm.newPassword" type="password" label="新密码" placeholder="新密码" :rules="[{ required: true, message: '新密码不能为空' }]"></van-field>
                            <van-field v-model="editPwdForm.confirmNewPassword" type="password" label="确认新密码" placeholder="确认新密码" :rules="[{ required: true, message: '确认新密码不能为空' }]"></van-field>
                            <div style="margin: 10px 0">
                                <van-button round block type="info" native-type="submit"> 修 改 </van-button>
                            </div>
                        </van-form>
                    </div>
                </van-col>
            </van-row>
        </van-popup>
    </div>
</div>

<script>
    new Vue({
        el: '#top',

        data() {
            return {
                editPwdShow: false,
                showShare: false,
                options: [
                    {
                        name: "退出帐号",
                        icon: "https://img.yzcdn.cn/vant/custom-icon-water.png",
                        key: "logout"
                    }, {
                        name: "修改密码",
                        icon: "https://img.yzcdn.cn/vant/custom-icon-water.png",
                        key: "editPwd"
                    }
                ],
                editPwdForm: {}
            }
        },
        created() {
        },
        methods: {
            onClickLeft() {
                window.history.go(-1)
            },
            onSelect(option) {
                const that = this;
                that.showShare = false;
                if (option.key === "logout") {
                    window.location = "/logout";
                    return;
                }
                if (option.key === 'editPwd') {
                    this.editPwdShow = true
                    return;
                }
            },
            onSubmit() {
                const that = this;
                axios.post('/api/editPwd', this.editPwdForm).then(res => {
                    const { code, message } = res.data
                    if (code === 200) {
                        this.$dialog.alert({
                            title: "提示",
                            message: '修改成功',
                        }).then(() => {
                            window.location = "/login";
                        });
                    } else {
                        this.$dialog.alert({
                            title: "提示",
                            message: message,
                        }).then(() => {});
                    }
                })
            },
        },
    });
</script>


<style scoped>
    .from {
        position: absolute;
        top: 45%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
</style>