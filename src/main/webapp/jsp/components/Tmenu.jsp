<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="menu">
  <el-tabs v-model="active" type="card" @tab-click="clickTab">
    <el-tab-pane v-for="item in options" :name="item.value">
      <span slot="label"><i :class="item.icon"></i> {{item.label}}</span>
    </el-tab-pane>
  </el-tabs>
</div>

<jsp:include page="../components/Ttop.jsp" flush="true">
  <jsp:param name="title" value="${param.title}" />
</jsp:include>
<br>

<script>
  new Vue({
    el: '#menu',

    data() {
      return {
        active: ${param.active}+'',
        options: [
          {
            label: "个人中心",
            value: '0',
            icon: 'el-icon-user',
            url: '/teacher/info'
          }, {
            label: "班级管理",
            value: '1',
            icon: 'el-icon-wind-power',
            url: '/teacher/grades'
          }, {
            label: "我的教学",
            value: '2',
            icon: 'el-icon-wind-power',
            url: '/teacher/timetable'
          },
          {
            label: "设 置",
            value: '3',
            icon: 'el-icon-setting',
            url: '/teacher/setting'
          },
        ],
      }
    },
    created() {

    },
    methods: {
        clickTab(tab, event) {
            window.location = (this.options.find(x=>x.value === tab.name)?.url)
        },
    },
  });
</script>