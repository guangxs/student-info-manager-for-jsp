<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="setting">
    <el-row :gutter="12">
        <el-col :span="12">
            <el-card class="box-card" shadow="hover">
                <el-form ref="form" :model="form" label-width="80px">
                    <el-form-item label="旧密码" >
                        <el-input v-model="form.originalPassword"></el-input>
                    </el-form-item>
                    <el-form-item label="新密码" >
                        <el-input v-model="form.newPassword"></el-input>
                    </el-form-item>
                    <el-form-item label="旧密码" >
                        <el-input v-model="form.confirmNewPassword"></el-input>
                    </el-form-item>
                    <el-form-item>
                        <el-button type="primary" @click="editPwd">修改密码</el-button>
                        <el-button type="primary" @click="logout">退出登陆</el-button>
                    </el-form-item>
                </el-form>
            </el-card>
        </el-col>
    </el-row>

</div>


<script>

    new Vue({
        el: '#setting',
        data() {
            return {
                form: {}
            }
        },
        created() {
        },
        methods: {
            logout(){
                window.location = "/logout";
            },
            editPwd() {
                axios.post('/api/editPwd', this.form).then(res=> {
                    const {code, message} = res.data
                    if (code === 200) {
                        this.$message({
                            message: '修改成功!',
                            type: 'success'
                        });
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            }
        },
    });
</script>