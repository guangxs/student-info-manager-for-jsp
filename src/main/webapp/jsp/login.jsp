<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/css/vant.css"/>
    <script type="text/javascript" src="/js/vue.js"></script>
    <script type="text/javascript" src="/js/vant.min.js"></script>
    <script type="text/javascript" src="/js/axios.min.js"></script>
    <title> 登 录 </title>
    <style scoped>
        .layout {
            /*flex-direction: row-reverse;*/
            background-image: url('/images/background1.jpg');
            height: 100vh;
        }

        .from {
            position: absolute;
            top: 45%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
    </style>
</head>
<body>

<div id="app" class="layout">
    <van-row>
        <van-col offset="7" span="10">
            <van-dropdown-menu>
                <van-dropdown-item v-model="loginForm.type" :options="options"></van-dropdown-item>
            </van-dropdown-menu>
        </van-col>
    </van-row>
    <van-row>
        <van-col offset="0" span="24">
            <div class="from">
                <van-form @submit="onSubmit">
                    <van-field left-icon="" v-model="loginForm.number" label="账号" :placeholder="options[loginForm.type].name" :rules="[{ required: true, message: '账户不能为空' }]"></van-field>
                    <van-field v-model="loginForm.password" type="password" label="密码" placeholder="密码" :rules="[{ required: true, message: '密码不能为空' }]"></van-field>
                    <div style="margin: 10px 0">
                        <van-button round block type="info" native-type="submit">登 录</van-button>
                    </div>
                </van-form>
            </div>
        </van-col>
    </van-row>
</div>

<script>

    new Vue({
        el: '#app',

        data() {
            return {
                loginForm: {
                    number: 'admin',
                    password: '123456',
                    type: 2,
                },
                options: [
                    {text: "管理员登录", name: "工号", value: 2},
                    {text: "教师登录", name: "工号", value: 1},
                    {text: "学生登录", name: "学号", value: 0},
                ],
            }
        },
        created() {
        },
        methods: {
            onSubmit() {
                const that = this;
                const from = that.loginForm;
                if (that.checkNull(from.number) || that.checkNull(from.password)) {
                    this.$dialog.alert({
                        title: "提示",
                        message: "请填写账号信息",
                    }).then(() => {});
                    return;
                }

                axios.post("/api/login", that.loginForm).then((res) => {
                    const { code, message } = res.data
                    if (code === 200) {
                        switch (that.loginForm.type) {
                            case 0:
                                window.location = "/student/info";
                                break;
                            case 1:
                                window.location = "/teacher/info";
                                break;
                            case 2:
                                window.location = "/admin/teachers";
                                break;
                            default :
                                window.location = "/student/info";
                                break;
                        }
                        return
                    }
                    this.$dialog.alert({
                        title: "提示",
                        message: message,
                    }).then(() => {});
                });
            },

            checkNull(value) {
                if (value === null || value === "" || typeof value === "undefined") {
                    return true;
                }
                return false;
            },
        },
    });
</script>

<%@include file="common/footer.jsp"%>