<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<jsp:include page="../common/Sheader.jsp" flush="true">
    <jsp:param name="title" value="${title}"/>
    <jsp:param name="active" value="0"/>
</jsp:include>

<div id="app">
    <van-search v-model="search" show-action placeholder="课程名/教师姓名/教学楼">
        <template #action>
            <div @click="onSearch">搜索</div>
        </template>
    </van-search>
    <van-empty v-if="timeTable === null" description="好清净啊" ></van-empty>
    <div v-else>
        <van-cell-group>
            <van-cell v-for="item in timeTable" :value="knob(item)" :label="item.room.level + item.room.room">
                <template #title>
                    <span class="custom-title">{{ item.course.name }}</span>
                    <van-tag type="danger">{{ item.teacher.name }}</van-tag>
                </template>
            </van-cell>
        </van-cell-group>
    </div>
</div>



<script>
    new Vue({
        el: '#app',

        data() {
            return {
                search: "",
                showPopover: false,
                timeTable: null,
                tables: [],
                grade: null,
            }
        },
        created() {
            this.onSearch()
        },
        methods: {
            onSearch() {
                axios.get('/api/timetable/studentTimetable/' + this.search).then(res => {
                    const { code, data, message } = res.data
                    if (code === 200) {
                        this.timeTable = data
                        return
                    }
                    this.$dialog.alert({
                        title: "提示",
                        message: message,
                    }).then(() => {});
                })
            },
            onClickLeft() {
            },
            getCourse(courseId) {
            },
            knob(item) {
                return '星期' + item.week + '-第' + item.knob + '节'
            }
        },
    });
</script>

<%@include file="../common/footer.jsp" %>