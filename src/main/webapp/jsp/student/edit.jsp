<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<jsp:include page="../common/Sheader.jsp" flush="true">
    <jsp:param name="title" value="${title}"/>
</jsp:include>

<script type="text/javascript" src="/js/DateUtil.js"></script>

<div id="app">
    <van-form @submit="onSubmit">
        <van-cell-group inset>
            <van-field v-model="form.nickname" name="昵称" label="昵称" placeholder="昵称"></van-field>
            <van-field name="radio" label="性别">
                <template #input>
                    <van-radio-group v-model="form.sex" direction="horizontal">
                        <van-radio name="男">男</van-radio>
                        <van-radio name="女">女</van-radio>
                    </van-radio-group>
                </template>
            </van-field>
            <van-field v-model="form.phone" label="电话" placeholder="电话"></van-field>
            <van-field v-model="form.email" label="邮箱" placeholder="邮箱"></van-field>
            <van-field readonly clickable name="currentDate" :value="form.birth" label="生辰" placeholder="点击选择" @click="showPicker = true"></van-field>
            <van-popup v-model="showPicker" position="bottom">
                <van-datetime-picker title="选择时间" type="date" :min-date="minDate" :max-date="maxDate" @confirm="onConfirm" @cancel="showPicker = false"></van-datetime-picker>
            </van-popup>
            <van-field readonly clickable name="area" :value="form.address" label="故乡" placeholder="点击选择省市区" @click="showArea = true"></van-field>

            <van-popup v-model="showArea" position="bottom">
                <van-area title="选择省市区" :area-list="citys" @confirm="getCity" @cancel="showArea = false"></van-area>
            </van-popup>
            <div style="margin: 16px;">
                <van-button round block type="info" native-type="submit" @click="onSubmit">提交</van-button>
            </div>
        </van-cell-group>
    </van-form>
</div>



<script>
    new Vue({
        el: '#app',

        data() {
            return {
                citys: [],
                showArea: false,
                value: "",
                showPicker: false,
                minDate: new Date(1950, 1, 1),
                maxDate: new Date(),
                avatar: [],
                form: {},
                id: ${id},
            }
        },
        created() {
            const that = this;
            axios.get("/json/Citys.json").then((res) => {
                that.citys = res.data;
            });
            axios.get('/api/student/info/' + that.id).then(res => {
                const {code, data, message} = res.data
                if (code === 200) {
                    this.form = data
                    return
                }
                this.$dialog.alert({
                    title: "提示",
                    message: message,
                }).then(() => {
                });
            })
        },
        methods: {
            onSubmit() {
                axios.post('/api/student/editMyInfo', this.form).then(res => {
                    const {code, message} = res.data
                    if (code === 200) {
                        window.location = "/student/info";
                        return
                    }
                    this.$dialog.alert({
                        title: "提示",
                        message: message,
                    }).then(() => {
                    });
                })
            },
            onConfirm(date) {
                this.form.birth = format("YYYY-mm-dd", date);
                this.showPicker = false;
            },
            getCity(values) {
                this.form.address = values
                    .filter((item) => !!item)
                    .map((item) => item.name)
                    .join("/");
                this.showArea = false;
            },
        },
    });
</script>

<%@include file="../common/footer.jsp" %>