<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<jsp:include page="../common/Sheader.jsp" flush="true">
    <jsp:param name="title" value="${title}"/>
    <jsp:param name="active" value="2"/>
</jsp:include>

<div id="app">
    <van-swipe-cell>
        <van-notice-bar v-show="wipeShow" left-icon="volume-o" scrollable :text="checkNull(grade.notice)"></van-notice-bar>
        <template #right>
            <van-button square type="danger" @click="wipeShow = false" text="暂时屏蔽"></van-button>
        </template>
    </van-swipe-cell>

    <van-divider>{{grade.number}} - {{grade.name}}</van-divider>

    <van-collapse v-model="actionNames">
        <div @click="show.teacher = !show.teacher">
            <van-sticky>
                <van-collapse-item size="large" title="班主任" name="teacher"></van-collapse-item>
            </van-sticky>
        </div>

        <div v-if="show.teacher">
            <van-card tag="班主任" :title="teacher.name" :desc="teacher.number" :thumb="teacher.avatar"></van-card>
            <van-divider :style="{color: '#1989fa',borderColor: '#1989fa',padding: '0 16px',}">
                <van-icon name="smile-o"></van-icon>
                <van-icon name="smile"></van-icon>
                <van-icon name="smile-o"></van-icon>
            </van-divider>
        </div>

        <div @click="show.students = !show.students">
            <van-sticky>
                <van-collapse-item size="large" title="班级学生" name="students"></van-collapse-item>
            </van-sticky>
        </div>

        <div v-if="show.students">
            <van-list :finished="true" finished-text="没有更多了">
                <van-cell v-for="student in grade.students">
                    <van-card :tag="student.position" :title="student.name" :desc="student.number" :thumb="student.avatar">
                        <template #footer>
                            <van-button size="small">
                                <van-icon name="good-job-o"></van-icon>
                            </van-button>
                            <van-button size="small" @click="lookInfo(student.id)">
                                <van-icon name="ellipsis"></van-icon>
                            </van-button>
                        </template>
                    </van-card>
                </van-cell>
            </van-list>
            <van-divider :style="{color: '#1989fa',borderColor: '#1989fa',padding: '0 16px',}">
                <van-icon name="smile-o"></van-icon>
                <van-icon name="smile"></van-icon>
                <van-icon name="smile-o"></van-icon>
            </van-divider>
        </div>

        <div @click="show.courses = !show.courses">
            <van-sticky>
                <van-collapse-item size="large" title="班级课程" name="courses"></van-collapse-item>
            </van-sticky>
        </div>
        <div v-if="show.courses">
            <van-list :finished="true" finished-text="没有更多了">
                <van-cell v-for="course in grade.courses">
                    <van-card :tag="course.learnTime + '学时'" :title="course.name" :desc="course.number" thumb="/images/ipad.jpeg"></van-card>
                </van-cell>
            </van-list>
            <van-divider :style="{color: '#1989fa',borderColor: '#1989fa',padding: '0 16px',}">
                <van-icon name="smile-o"></van-icon>
                <van-icon name="smile"></van-icon>
                <van-icon name="smile-o"></van-icon>
            </van-divider>
        </div>
    </van-collapse>
    <div v-if="studentShow">
        <van-popup v-model="studentShow" position="bottom" round duration="1" :style="{ height: '80%' }" closeable close-icon="arrow-down">
            <div>
                <van-image round width="10rem" height="10rem" fit="cover" :src="checkNull(student.avatar)"></van-image>
                <van-contact-card type="edit" :name="checkNull(student.name)" :tel="checkNull(student.phone)"></van-contact-card>
                <van-cell title="学号" :value="checkNull(student.number)" icon="award-o"></van-cell>
                <van-cell title="专业" :value="checkNull(student.major)" icon="cashier-o"></van-cell>
                <van-cell title="职务" :value="checkNull(student.position)" icon="records"></van-cell>
                <van-cell title="性别" :value="checkSex(student.sex)" icon="manager-o"></van-cell>
                <van-cell title="昵称" :value="checkNull(student.nickname)" icon="label-o"></van-cell>
                <van-cell title="住址" :value="checkNull(student.address)" icon="location-o"></van-cell>
                <van-cell title="邮箱" :value="checkNull(student.email)" icon="envelop-o"></van-cell>
                <van-cell title="生日" :value="checkNull(student.birth)" icon="gift-card-o"></van-cell>
            </div>
        </van-popup>
    </div>
</div>

<script>
    new Vue({
        el: '#app',

        data() {
            return {
                grade: {},
                wipeShow: true,
                teacher: {},
                student: {},
                show: {
                    teacher: false,
                    students: false,
                    courses: false,
                },
                actionNames: [],

                studentShow: false,
            }
        },
        created() {
            axios.get('/api/grade/myGradeInfo').then(res => {
                const {code, data, message} = res.data
                if (code === 200) {
                    console.log(data)
                    this.grade = data;
                    this.teacher = this.grade.teacher;
                    return
                }
                this.$dialog.alert({
                    title: "提示",
                    message: message,
                }).then(() => {
                });
            })
        },
        methods: {
            lookInfo(id) {
                const that = this
                const students = that.grade.students
                for (i in students) {
                    if (students[i].id === id) {
                        that.student = students[i];
                        that.studentShow = true;
                    }
                }
            },
            checkNull(value) {
                const that = this;
                if (value === null) {
                    return "无信息";
                }
                return value;
            },
            checkSex(value) {
                value = this.checkNull(value);
                if (value === "无信息") {
                    return value;
                }
                if (value === "男") {
                    value = "男";
                }
                if (value === "女") {
                    value = "女";
                }
                return value;
            },
        },
    });

</script>

<%@include file="../common/footer.jsp" %>