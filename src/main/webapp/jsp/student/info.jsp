<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<jsp:include page="../common/Sheader.jsp" flush="true">
	<jsp:param name="title" value="${title}" />
	<jsp:param name="active" value="3"/>
</jsp:include>

<div id="app">
	<van-image round width="10rem" height="10rem" fit="cover" :src="student.avatar"></van-image>
	<van-contact-card type="edit" :name="student.name" :tel="student.phone" @click="onEdit"></van-contact-card>
	<van-cell title="学号" :value="student.number" icon="award-o"></van-cell>
	<van-cell title="专业" :value="student.major" icon="cashier-o"></van-cell>
	<van-cell title="职务" :value="student.position" icon="records"></van-cell>
	<van-cell title="性别" :value="student.sex" icon="manager-o" ></van-cell>
	<van-cell title="昵称" :value="student.nickname" icon="label-o"></van-cell>
	<van-cell title="住址" :value="student.address" icon="location-o"></van-cell>
	<van-cell title="邮箱" :value="student.email" icon="envelop-o"></van-cell>
	<van-cell title="生日" :value="student.birth" icon="gift-card-o"></van-cell>
</div>


<script>
	new Vue({
		el: '#app',
		data() {
			return {
				student: {},
			}
		},
		created() {
			axios.get('/api/student/myInfo').then(res => {
				const { code, data, message } = res.data
				if (code === 200) {
					this.student = data
					return
				}
				this.$dialog.alert({
					title: "提示",
					message: message,
				}).then(() => {});
			})
		},
		methods: {
			onEdit() {
				window.location = "/student/edit";
			},
		},
	});
</script>

<%@include file="../common/footer.jsp"%>