<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<jsp:include page="../common/Sheader.jsp" flush="true">
    <jsp:param name="title" value="${title}"/>
    <jsp:param name="active" value="1"/>
</jsp:include>

<div id="app">
    <van-search v-model="name" show-action placeholder="课程名">
        <template #action>
            <div @click="onSearch">搜索</div>
        </template>
    </van-search>

    <van-empty v-if="scores === null" description="好清净啊"></van-empty>
    <div v-else>
        <van-cell-group>
            <van-cell v-for="score in scores" :title="score.course.name" :value="getScore(score.score)" size="large"></van-cell>
        </van-cell-group>
    </div>
</div>



<script>
    new Vue({
        el: '#app',

        data() {
            return {
                name: "",
                scores: [],
            }
        },
        created() {
            this.onSearch()
        },
        methods: {
            getScore(s){
                if (s < 60){
                    return '不及格'
                } return s + '分'
            },
            onSearch() {
                axios.get('/api/achievenmet/studentAchievement/' + this.name).then(res => {
                    const { code, data, message } = res.data
                    if (code === 200) {
                        this.scores = data
                        return
                    }
                    this.$dialog.alert({
                        title: "提示",
                        message: message,
                    }).then(() => {});
                })
            },
        },
    });

</script>

<%@include file="../common/footer.jsp" %>