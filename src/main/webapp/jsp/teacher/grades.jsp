<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<jsp:include page="../common/Theader.jsp" flush="true">
    <jsp:param name="title" value="${title}"/>
    <jsp:param name="active" value="1"/>
</jsp:include>

<div id="app">
    <el-card class="box-card" shadow="hover">
        <el-empty v-if="grades.length === 0" :image-size="200" description="您暂时还没有管理班级哦，注意：班级是管理员创建并分配给教师的"/>
        <el-collapse v-model="activeGrade" accordion>
            <el-collapse-item v-for="grade in grades" :title="grade.name" :name="grade.id">
                <el-card class="box-card" shadow="hover">
                    <el-collapse v-model="activeGradeItem" accordion>
                        <el-collapse-item title="班级学生" :name="'students' + grade.id">
                            <el-card class="box-card" shadow="hover">
                                <span style="font-size: 20px">
                                    公告：<el-tag><span style="font-size: 20px">{{grade.notice}}</span></el-tag>
                                    <el-button type="primary" icon="el-icon-edit" size="small" @click="editNotice(grade)"></el-button>
                                    班级号：<el-tag><span style="font-size: 20px">{{grade.number}} </span></el-tag>
                                    <el-button type="primary" style="margin-left: 10px" icon="el-icon-refresh" @click="gradesData"></el-button>
                                    <el-button type="primary" icon="el-icon-plus" @click="handleInsertStudent(grade)">添加学生</el-button>
                                </span>

                                <el-table :data="grade.students" style="width: 100%">
                                    <el-table-column prop="number" label="学号"></el-table-column>
                                    <el-table-column prop="major" label="专业"></el-table-column>
                                    <el-table-column prop="name" label="姓名"></el-table-column>
                                    <el-table-column prop="position" label="职位"></el-table-column>
                                    <el-table-column prop="avatar" label="头像">
                                        <template slot-scope="scope">
                                            <el-avatar :src="scope.row.avatar"></el-avatar>
                                        </template>
                                    </el-table-column>
                                    <el-table-column prop="nickname" label="昵称"></el-table-column>
                                    <el-table-column prop="sex" label="性别"></el-table-column>
                                    <el-table-column prop="email" label="邮箱"></el-table-column>
                                    <el-table-column prop="phone" label="电话"></el-table-column>
                                    <el-table-column prop="address" label="地址"></el-table-column>
                                    <el-table-column prop="birth" label="生日"></el-table-column>
                                    <el-table-column label="操作">
                                        <template slot-scope="scope">
                                            <el-button size="mini" @click="handleEditStudent(scope.row)">编辑</el-button>
                                            <el-button size="mini" type="danger"
                                                       @click="handleDeleteStudent(scope.row)">删除
                                            </el-button>
                                        </template>
                                    </el-table-column>
                                </el-table>
                            </el-card>
                        </el-collapse-item>
                        <el-collapse-item title="班级课程" :name="'course' + grade.id">
                            <el-card class="box-card" shadow="hover">
                                <el-table :data="grade.courses" style="width: 100%">
                                    <el-table-column prop="number" label="课程号"></el-table-column>
                                    <el-table-column prop="name" label="课程名"></el-table-column>
                                    <el-table-column prop="learnTime" label="学时"></el-table-column>
                                    <el-table-column prop="credit" label="学分"></el-table-column>
                                </el-table>
                            </el-card>
                        </el-collapse-item>
                    </el-collapse>
                </el-card>
            </el-collapse-item>
        </el-collapse>
    </el-card>

    <el-drawer :title="studentForm.id === undefined ? '添加学生信息' : '修改学生信息'" :visible.sync="drawerEditStudent"
               direction="rtl" size="50%">
        <el-card class="box-card" shadow="hover">
            <el-form ref="studentForm" :model="studentForm" label-width="80px">
                <el-form-item label="学生姓名">
                    <el-input v-model="studentForm.name"></el-input>
                </el-form-item>
                <el-form-item label="学生学号">
                    <el-input v-model="studentForm.number"></el-input>
                </el-form-item>
                <el-form-item label="学生专业">
                    <el-input v-model="studentForm.major"></el-input>
                </el-form-item>
                <el-form-item label="学生职位">
                    <el-radio-group v-model="studentForm.position">
                        <el-radio label="学生"></el-radio>
                        <el-radio label="班长"></el-radio>
                        <el-radio label="团委"></el-radio>
                    </el-radio-group>
                </el-form-item>
                <el-form-item label="学生性别">
                    <el-radio-group v-model="studentForm.sex">
                        <el-radio label="男"></el-radio>
                        <el-radio label="女"></el-radio>
                    </el-radio-group>
                </el-form-item>
                <el-form-item label="学生昵称">
                    <el-input v-model="studentForm.nickname"></el-input>
                </el-form-item>
                <el-form-item>
                    <el-button type="primary" @click="saveOrUpdateStudent">提 交</el-button>
                    <el-button>取 消</el-button>
                </el-form-item>
            </el-form>
        </el-card>
    </el-drawer>
</div>


<script>
    new Vue({
        el: '#app',
        data() {
            return {
                drawerEditStudent: false,
                activeGrade: '',
                activeGradeItem: '',
                grades: [],
                editForm: {},
                studentForm: {},
            }
        },
        created() {
            this.gradesData()
        },
        methods: {
            editNotice(grade) {
                let that = this
                that.editForm.id = grade.id
                this.$prompt('当前公告显示如下', '修改班级公告', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    inputValue: grade.notice,
                }).then(({value}) => {
                    that.editForm.notice = value
                    axios.post('/api/grade/editNotice', that.editForm).then(res => {
                        const {code, message, data} = res.data
                        if (code === 200) {
                            grade.notice = data.notice
                        } else {
                            this.$message({
                                type: 'error',
                                message: message
                            });
                        }
                    })
                });

            },
            handleEditStudent(row) {
                this.studentForm = row
                this.drawerEditStudent = true
            },
            handleInsertStudent(grade) {
                this.studentForm = {}
                this.studentForm.gradeId = grade.id
                this.drawerEditStudent = true
            },
            saveOrUpdateStudent() {
                axios.post('/api/student/saveOrUpdate', this.studentForm).then(res => {
                    const {code, data, message} = res.data
                    if (code === 200) {
                        this.gradesData()
                        this.$message({
                            message: message,
                            type: 'success'
                        });
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            },
            handleDeleteStudent(row) {
                axios.delete('/api/student/delete/' + row.id).then(res => {
                    const {code, message} = res.data
                    if (code === 200) {
                        this.gradesData()
                        this.$message({
                            message: '学生' + row.name + '成功删除！',
                            type: 'success'
                        });
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            },
            gradesData() {
                axios.post('/api/teacher/grades').then(res => {
                    const {code, data, message} = res.data
                    if (code === 200) {
                        this.grades = data
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            }
        },
    });
</script>