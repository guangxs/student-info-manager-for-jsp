<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<jsp:include page="../common/Theader.jsp" flush="true">
    <jsp:param name="title" value="${title}"/>
    <jsp:param name="active" value="0"/>
</jsp:include>


<div id="app">
    <el-row :gutter="12">
        <el-col :span="12">
            <el-card class="box-card" shadow="hover">
                <div slot="header" class="clearfix">
                    <span>个人信息</span>
                    <el-button style="float: right; padding: 3px 0" type="text" @click="myInfo">刷 新</el-button>
                </div>
                <div>
                    <el-col :span="24">
                        <div align="center">
                            <el-avatar shape="square" :size="50" :src="teacher.avatar"></el-avatar>
                        </div>
                    </el-col>
                    <ul style="list-style: none;color: #409EFF; font-size: 20px;">
                        <li style="margin-top: 10px"><i class="el-icon-user"></i> 姓名：{{ teacher.name }}</li>
                        <li style="margin-top: 10px"><i class="el-icon-s-check"></i> 工号：{{ teacher.number }}</li>
                        <li style="margin-top: 10px"><i class="el-icon-s-cooperation"></i> 职称：{{ teacher.leader }}</li>
                        <li style="margin-top: 10px"><i class="el-icon-male"></i> 性别：{{ teacher.sex }}</li>
                        <li style="margin-top: 10px"><i class="el-icon-notebook-1"></i> 电话：{{ teacher.phone }}</li>
                        <li style="margin-top: 10px"><i class="el-icon-location-outline"></i> 住址：{{ teacher.address }}
                        </li>
                        <li style="margin-top: 10px"><i class="el-icon-present"></i> 生日：{{ teacher.birth }}</li>
                    </ul>
                </div>
            </el-card>
        </el-col>
        <el-col :span="12">
            <el-card class="box-card" shadow="hover">
                <div slot="header" class="clearfix">
                    <span>修改信息</span>
                    <el-button style="float: right; padding: 3px 0" type="text" @click="editMyInfo">提 交</el-button>
                </div>
                <div>
                    <el-form ref="teacherForm" :model="teacherForm" label-width="80px">
                        <el-form-item label="姓名">
                            <el-input v-model="teacherForm.name"></el-input>
                        </el-form-item>
                        <el-form-item label="电话">
                            <el-input v-model="teacherForm.phone"></el-input>
                        </el-form-item>
                        <el-form-item label="性别">
                            <el-radio-group v-model="teacherForm.sex">
                                <el-radio label="男"></el-radio>
                                <el-radio label="女"></el-radio>
                            </el-radio-group>
                        </el-form-item>
                        <el-form-item label="住址">
                            <el-cascader v-model="cityValue" :options="citys" :props="{ expandTrigger: 'hover' }"
                                         @change="handleChange"></el-cascader>
                        </el-form-item>
                        <el-form-item label="生日">
                            <el-date-picker type="date" placeholder="选择日期" v-model="teacherForm.birth"
                                            style="width: 100%;"></el-date-picker>
                        </el-form-item>
                    </el-form>
                </div>
            </el-card>
        </el-col>
    </el-row>
</div>


<script>
    new Vue({
        el: '#app',
        data() {
            return {
                teacher: {},
                teacherForm: {},
                cityValue: [],
                citys: [],
            }
        },
        created() {
            const that = this;
            that.myInfo();
            axios.get("/json/Citys.json").then((res) => {
                let citys = res.data
                let province_list = citys.province_list;
                let city_list = citys.city_list
                let county_list = citys.county_list
                let ic = []
                //城市数据转化
                for (let pk in province_list) {
                    let province = {
                        value: pk,
                        label: province_list[pk],
                        children: []
                    }
                    let pkt = parseInt(pk / 10000)
                    for (let ck in city_list) {
                        let city = {
                            value: ck,
                            label: city_list[ck],
                            children: []
                        }
                        let ckpkt = parseInt(ck / 10000)
                        if (pkt === ckpkt) {
                            let ckt = parseInt((ck / 100) % 100)
                            for (let k in county_list) {
                                let county = {
                                    value: k,
                                    label: county_list[k]
                                }
                                let kckt = parseInt((k / 100) % 100)
                                if (ckt === kckt) {
                                    city.children.push(county)
                                }
                            }
                            province.children.push(city)
                        }
                    }
                    ic.push(province)
                }
                this.citys = ic
            });
        },
        methods: {
            editMyInfo() {
                axios.post('/api/teacher/editMyInfo', this.teacherForm).then(res => {
                    const {code, message} = res.data
                    if (code === 200) {
                        this.$message({
                            message: '修改成功',
                            type: 'success'
                        });
                        return
                    }
                    this.$message({
                        message: message,
                        type: 'error'
                    });
                })
            },
            handleChange(value) {
                const that = this;
                const citys = that.citys
                let city = ''
                for (let i in citys) {
                    if (citys[i].value === value[0]) {
                        city += citys[i].label
                        for (let j in citys[i].children) {
                            if (citys[i].children[j].value === value[1]) {
                                city += citys[i].children[j].label
                                for (let m in citys[i].children[j].children) {
                                    if (citys[i].children[j].children[m].value === value[2]) {
                                        city += citys[i].children[j].children[m].label
                                    }
                                }
                            }
                        }
                    }
                }
                this.teacherForm.address = city
            },
            myInfo() {
                axios.get('/api/teacher/myInfo').then(res => {
                    const {code, data, message} = res.data
                    if (code === 200) {
                        this.teacher = data

                        this.teacherForm = Object.assign({}, data);
                        return
                    }
                    this.$message({
                        message: message,
                        type: 'error'
                    });
                })
            }
        },
    });
</script>

<%@include file="../common/footer.jsp" %>