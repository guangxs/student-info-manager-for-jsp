<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<jsp:include page="../common/Theader.jsp" flush="true">
    <jsp:param name="title" value="${title}"/>
    <jsp:param name="active" value="3"/>
</jsp:include>

<jsp:include page="../components/setting.jsp" flush="true"/>

<div id="app">

</div>

<script>
    new Vue({
        el: '#app',
        data() {
            return {
            }
        },
        created() {
        },
        methods: {

        },
    });
</script>

<%@include file="../common/footer.jsp" %>