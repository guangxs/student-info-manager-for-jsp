<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<jsp:include page="../common/Theader.jsp" flush="true">
    <jsp:param name="title" value="${title}" />
    <jsp:param name="active" value="2"/>
</jsp:include>

<div id="app">
    <el-card class="box-card" shadow="hover">
        <el-collapse v-model="activeTimetable" accordion>
            <%--<el-collapse-item  title="我的课程" name="activeTimetable">

            </el-collapse-item>--%>
                <el-table :data="timetables" style="width: 100%">
                    <el-table-column prop="course.name" label="课程名"></el-table-column>
                    <el-table-column prop="course.number" label="课程号"></el-table-column>
                    <el-table-column prop="course.learnTime" label="课程学时"></el-table-column>
                    <el-table-column prop="course.credit" label="课程学分"></el-table-column>
                    <el-table-column prop="grade.name" label="上课班级"></el-table-column>
                    <el-table-column prop="grade.number" label="班级号"></el-table-column>
                    <el-table-column prop="grade.students.length" label="学生人数"></el-table-column>
                    <el-table-column prop="room.level" label="教学楼"></el-table-column>
                    <el-table-column prop="room.room" label="教室"></el-table-column>
                    <el-table-column prop="room.number" label="授课教室号"></el-table-column>
                    <el-table-column prop="start" label="授课起始"></el-table-column>
                    <el-table-column prop="end" label="授课截止"></el-table-column>
                    <el-table-column prop="week" label="授课周"></el-table-column>
                    <el-table-column prop="knob" label="授课时间"></el-table-column>
                    <el-table-column label="操作">
                        <template slot-scope="scope">
                            <el-button size="mini" type="danger" @click="studentAchievement(scope.row)">成绩</el-button>
                        </template>
                    </el-table-column>
                </el-table>
        </el-collapse>
    </el-card>

    <el-drawer :title="course.name + '课程成绩'" :visible.sync="drawerStudentAchievement" direction="rtl" size="50%">
        <el-card class="box-card" shadow="hover">
                <el-table :data="students"  style="width: 100%">
                    <el-table-column prop="name" label="学生姓名"></el-table-column>
                    <el-table-column prop="number" label="学生学号"></el-table-column>
                    <el-table-column label="成绩">
                        <template slot-scope="scope">
                            <span>分数：{{scope.row.scoreo}}</span>
                            <el-input v-model="scope.row.score" type="number"></el-input>
                        </template>
                    </el-table-column>
                    <el-table-column label="操作">
                        <template slot-scope="scope">
                            <el-button type="primary" @click="saveOrUpdateStudentAchievement(scope.row, course.id)">提 交</el-button>
                        </template>
                    </el-table-column>

                </el-table>
        </el-card>
    </el-drawer>
</div>

<script>
    new Vue({
        el: '#app',
        data() {
            return {
                timetables: [],
                activeTimetable: '',
                drawerStudentAchievement: false,
                achievementForm: [],
                students: [],
                course: {},
                achievements: [],
            }
        },
        created() {
            this.getTimetable()
        },
        methods: {
            getTimetable() {
                axios.get('/api/timetable/teacherTimetable').then(res => {
                    const {code, data, message} = res.data
                    if (code === 200) {
                        this.timetables = data
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            },
            getAchievement(courseId, students) {
                for (let i in students) {
                    axios.get('/api/achievenmet/teacherAchievement/' + courseId + '/' + students[i].id).then(res => {
                        const {code, data} = res.data
                        if (code === 200) {
                            if (data !== null) {
                                for (let j in this.students) {
                                    if (this.students[j].id === students[i].id) {
                                        this.students[j].scoreo = parseInt(data.score)
                                    }
                                }
                            }
                        }
                    })
                }
            },
            studentAchievement(row){
                this.students = row.grade.students
                this.course = row.course
                this.getAchievement(this.course.id, this.students)
                const that = this
                const loading = this.$loading({
                    lock: true,
                    text: 'Loading',
                    spinner: 'el-icon-loading',
                    background: 'rgba(0, 0, 0, 0.7)'
                });
                setTimeout(() => {
                    loading.close();
                    that.drawerStudentAchievement = true
                }, 1000);
            },
            saveOrUpdateStudentAchievement(row, courseId) {
                let form = {
                    score: row.score,
                    courseId: courseId,
                    studentId: row.id
                }
                axios.post('/api/achievenmet/saveAchievement', form).then(res => {
                    const {code, message} = res.data
                    if (code === 200) {
                        this.$message({
                            message: '打分成功!',
                            type: 'success'
                        });
                    } else {
                        this.$message({
                            message: message,
                            type: 'error'
                        });
                    }
                })
            }
        },
    });
</script>

<%@include file="../common/footer.jsp"%>
